import numpy as np
import matplotlib.pyplot as plt

def js_plot_eeg(eeg_sample, sample_offset, sampling_frequency, title, ch_names, output_dir, window_size, overlapping, fmin, fmax):
    
        vspace=0.01
        color='k'
        
        num_channels = eeg_sample.shape[0]
        num_time_steps = eeg_sample.shape[1]

        bases = np.flip(vspace * np.arange(num_channels))
        data = (eeg_sample.T + bases)

        #self.w(f"PLOT EEG DATA - type {type(data)} - shape {data.shape}")

        time = (np.arange(num_time_steps) / sampling_frequency) + ((sample_offset-1)*window_size)

        fig_a = plt.figure(figsize=[14, 14])
        plt.margins(x=0)
        # Plot EEG versus time
        plt.plot(time, data, color=color, linewidth=0.5)

        # Add gridlines to the plot
        plt.grid()

        # Label the axes
        plt.xlabel('Time (s)')
        plt.ylabel('Channels')
        
        # The y-ticks are set to the locations of the electrodes. The international 10-20 system defines
        # default names for them.
        plt.gca().yaxis.set_ticks(bases)
        plt.gca().yaxis.set_ticklabels(ch_names)
        
        # Put a nice title on top of the plot
        plt.title(title)

        fig_a.set_tight_layout(True)

        #fig_a.savefig(path.join(output_dir, f"time_{sample_offset:02d}.png"), dpi=300.)
        plt.show()
        plt.close(fig_a)