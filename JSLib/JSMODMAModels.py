import tensorflow as tf

class MultiScaleLSTM_block(tf.keras.layers.Layer):
  
    def __init__(self,p, kernel_size, block_name):

        super(MultiScaleLSTM_block, self).__init__()

        self._init_params(p)

        self.lstm_01=tf.keras.layers.LSTM(self.lstm_units, dropout=self.dropout_rate, return_sequences=True)
        self.norm_lstm=tf.keras.layers.BatchNormalization()
        self.lstm_02=tf.keras.layers.LSTM(self.lstm_units, dropout=self.dropout_rate)

        self.dropout = tf.keras.layers.Dropout(self.dropout_rate)

    def call(self, input_tensor, training=False):

        x_lstm=self.lstm_01(input_tensor, training=training)
        x_lstm=self.norm_lstm(x_lstm, training=training)
        x_lstm=self.lstm_02(x_lstm, training=training)

        x=self.dropout(x_lstm)

        return x


    def _init_params(self, p):

        self.p=p
        self.batch_normalization=(p.parameters["batch_normalization"]=='1')
        self.filters=int(p.parameters["filters"]) 
        # self.kernel_size=int(p.parameters["kernel_size"])
        self.padding=p.parameters["padding"]
        self.depth_multiplier=int(p.parameters["depth_multiplier"])

        if p.parameters["conv_depthwise_constraint"]=='None':
            self.conv_depthwise_constraint=None
        else:
            self.conv_depthwise_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["conv_depthwise_constraint"])
            )

        if p.parameters["conv_depthwise_regularizer"]=='None':
            self.conv_depthwise_regularizer=None
        else:
            self.conv_depthwise_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["conv_depthwise_regularizer"])
            )


        self.activation_conv=p.parameters["activation_conv"]
        self.use_bias=bool(int(p.parameters["use_bias"]))

        self.lstm_units=int(p.parameters["lstm_units"])

        self.dropout_rate=float(p.parameters["dropout_rate"])

        self.pool_size=int(p.parameters["pool_size"])

        self.conv_dense_units=int(p.parameters["conv_dense_units"])
        self.conv_dense_activation=p.parameters["conv_dense_activation"]

        self.block_dense_units=int(p.parameters["block_dense_units"])
        self.block_dense_activation=p.parameters["block_dense_activation"]

        # print(f"PADDING: [{self.padding}]\nDM [{p.parameters['depth_multiplier']}][{int(p.parameters['depth_multiplier'])}][{self.depth_multiplier}]\n DC [{self.conv_depthwise_constraint}]\n DC [{self.conv_depthwise_regularizer}]\n ACTIVATION [{self.activation_conv}]\n BIAS [{self.use_bias}]")
        


class MultiScaleCNN_LSTM_block(tf.keras.layers.Layer):
  
    def __init__(self,p, kernel_size, block_name):

        super(MultiScaleCNN_LSTM_block, self).__init__()

        self._init_params(p)

        if(self.batch_normalization):
            self.batch_norm_01=tf.keras.layers.BatchNormalization()


        self.conv_01=tf.keras.layers.SeparableConv1D(
                    filters=self.filters, 
                    kernel_size=kernel_size, 
                    padding=self.padding,
                    depth_multiplier=self.depth_multiplier,
                    depthwise_constraint=self.conv_depthwise_constraint,
                    depthwise_regularizer=self.conv_depthwise_regularizer,
                    activation=None,
                    use_bias=self.use_bias
                )
        self.conv_norm_01=tf.keras.layers.BatchNormalization()

        self.conv_02=tf.keras.layers.SeparableConv1D(
                    filters=self.filters//2, 
                    kernel_size=kernel_size, 
                    padding=self.padding,
                    depth_multiplier=self.depth_multiplier,
                    depthwise_constraint=self.conv_depthwise_constraint,
                    depthwise_regularizer=self.conv_depthwise_regularizer,
                    activation=None,
                    use_bias=self.use_bias
                )
        self.conv_norm_02=tf.keras.layers.BatchNormalization()
        self.conv_activation=tf.keras.layers.Activation(self.activation_conv)
        self.conv_pool=tf.keras.layers.MaxPool1D(pool_size=self.pool_size)
        self.conv_flat=tf.keras.layers.Flatten()
        self.conv_dense=tf.keras.layers.Dense(self.conv_dense_units, activation=self.conv_dense_activation)
        
        self.lstm_01=tf.keras.layers.LSTM(self.lstm_units, dropout=self.dropout_rate, return_sequences=True)
        self.lstm_02=tf.keras.layers.LSTM(self.lstm_units, dropout=self.dropout_rate)
        
        self.block_concat=tf.keras.layers.Concatenate()
        self.block_dense=tf.keras.layers.Dense(self.block_dense_units, activation=self.block_dense_activation)
        self.dropout = tf.keras.layers.Dropout(self.dropout_rate)

    def call(self, input_tensor, training=False):
        

        if(self.batch_normalization):
            x=self.batch_norm_01(x, training=training)

        x_conv=self.conv_01(input_tensor)
        x_conv=self.conv_norm_01(x_conv, training=training)
        x_conv=self.conv_02(x_conv)
        x_conv=self.conv_norm_02(x_conv, training=training)
        x_conv=self.conv_activation(x_conv)
        x_conv=self.conv_pool(x_conv)
        x_conv=self.conv_flat(x_conv)
        x_conv=self.conv_dense(x_conv)

        x_lstm=self.lstm_01(input_tensor, training=training)
        x_lstm=self.lstm_02(x_lstm, training=training)

        x_concat=self.block_concat([x_conv, x_lstm])
        x_dense=self.block_dense(x_concat)

        x=self.dropout(x_dense)

        return x


    def _init_params(self, p):

        self.p=p
        self.batch_normalization=(p.parameters["batch_normalization"]=='1')
        self.filters=int(p.parameters["filters"]) 
        # self.kernel_size=int(p.parameters["kernel_size"])
        self.padding=p.parameters["padding"]
        self.depth_multiplier=int(p.parameters["depth_multiplier"])

        if p.parameters["conv_depthwise_constraint"]=='None':
            self.conv_depthwise_constraint=None
        else:
            self.conv_depthwise_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["conv_depthwise_constraint"])
            )

        if p.parameters["conv_depthwise_regularizer"]=='None':
            self.conv_depthwise_regularizer=None
        else:
            self.conv_depthwise_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["conv_depthwise_regularizer"])
            )


        self.activation_conv=p.parameters["activation_conv"]
        self.use_bias=bool(int(p.parameters["use_bias"]))

        self.lstm_units=int(p.parameters["lstm_units"])

        self.dropout_rate=float(p.parameters["dropout_rate"])

        self.pool_size=int(p.parameters["pool_size"])

        self.conv_dense_units=int(p.parameters["conv_dense_units"])
        self.conv_dense_activation=p.parameters["conv_dense_activation"]

        self.block_dense_units=int(p.parameters["block_dense_units"])
        self.block_dense_activation=p.parameters["block_dense_activation"]

        # print(f"PADDING: [{self.padding}]\nDM [{p.parameters['depth_multiplier']}][{int(p.parameters['depth_multiplier'])}][{self.depth_multiplier}]\n DC [{self.conv_depthwise_constraint}]\n DC [{self.conv_depthwise_regularizer}]\n ACTIVATION [{self.activation_conv}]\n BIAS [{self.use_bias}]")
        

class MultiScaleCNN_LSTM(tf.keras.Model):

    def __init__(self, p):
        super(MultiScaleCNN_LSTM, self).__init__()

        self._init_params(p)

        self.permute = tf.keras.layers.Permute((2,1))
            
        self.block_list=[]
        for num_block in range(self.num_pb):
            div=2**num_block
        
            self.block_list.append( 
                # MultiScaleCNN_LSTM_block(
                MultiScaleLSTM_block(
                    self.p,
                    kernel_size=self.kernel_size//div,
                    block_name=f"PB_{num_block:02}"
                )
            )

        # self.block=MultiScaleCNN_LSTM_block(
        #     self.p, 
        #     kernel_size=self.kernel_size, 
        #     block_name=f"PB")
        
        if self.concat_mode=="AVG":
            self.concatBlock = tf.keras.layers.Average(name='Concat_Blocks_Average')
        elif self.concat_mode=="MAX":
            self.concatBlock = tf.keras.layers.Maximum(name='Concat_Blocks_Maximum')
        elif self.concat_mode=="MIN":
            self.concatBlock = tf.keras.layers.Minimum(name='Concat_Blocks_Minimum')
        elif self.concat_mode=="ALL":
            self.concatBlock_avg=tf.keras.layers.Average(name='Concat_Blocks_Average')
            self.concatBlock_max=tf.keras.layers.Maximum(name='Concat_Blocks_Maximum')
            self.concatBlock_min=tf.keras.layers.Minimum(name='Concat_Blocks_Minimum')
            self.concatBlock=tf.keras.layers.Concatenate(name='Concat_Blocks_Concatenate')
        else:
            self.concatBlock=tf.keras.layers.Concatenate(name='Concat_Blocks_Concatenate')


        self.drop = tf.keras.layers.Dropout(self.dropout_rate)

        self.classifier = tf.keras.layers.Dense(
            1, 
            use_bias=self.use_bias,
            kernel_constraint=self.dense_classifier_kernel_constraint,
            kernel_regularizer=self.dense_classifier_kernel_regularizer,
            )
        
        self.final_activation=tf.keras.layers.Activation('sigmoid')

    def call(self, input_tensor):

        x_permute = self.permute(input_tensor)
        
        x_out_pb_list=[]
        for pb in self.block_list:
            x_out_pb_list.append(pb(x_permute))

        if self.p.parameters["concat_mode"]=="ALL":
            x_concat_avg=self.concatBlock_avg(x_out_pb_list)
            x_concat_min=self.concatBlock_min(x_out_pb_list)
            x_concat_max=self.concatBlock_max(x_out_pb_list)
            x_concat=self.concatBlock([x_concat_min, x_concat_avg, x_concat_max])
        else:
            x_concat=self.concatBlock(x_out_pb_list)

        x=self.drop(x_concat)
        x=self.classifier(x)
        x=self.final_activation(x)
        
        return x
    
    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
    def _init_params(self, p):
        self.p=p

        self.num_pb=int(p.parameters["num_pb"])
        self.kernel_size=int(p.parameters["kernel_size"])
        self.concat_mode=p.parameters["concat_mode"]
        
        self.use_bias=bool(int(p.parameters["use_bias"]))
        self.dropout_rate=float(p.parameters["dropout_rate"])

        if p.parameters["dense_classifier_kernel_constraint"]=='None':
            self.dense_classifier_kernel_constraint=None
        else:
            self.dense_classifier_kernel_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["dense_classifier_kernel_constraint"]))

        if p.parameters["dense_classifier_kernel_regularizer"]=='None':
                self.dense_classifier_kernel_regularizer=None
        else:
            self.dense_classifier_kernel_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["dense_classifier_kernel_regularizer"])
            )

