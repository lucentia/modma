# Copyright (c) [2022] [Javier Sanchis; Sandra Ponsoda; XSB DISSENY I MULTIMEDIA, S.L.; LUCENTIA Lab]

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import mne
import os
import time
import gc
import math

import numpy as np

import mne_features.univariate as feat
import matplotlib.pyplot as plt

from scipy import signal
from stockwell import st
from JSLib.JSLog import JSLog
from PIL import Image

from sklearn.preprocessing import StandardScaler

class JSPreprocessingAdhd:

    def __init__(self, log_title = ""):

        self.jsLog = JSLog(f"JSPreprocessingAdhd_log_{log_title}")
        # Stores the statistics for each emage of each channel of each sample of each subject twice, one for the original s-transform, two for the resized s_transform
        # self.s_transform_statistics = []
        self.s_transform_statistics_header = [
                'index_subject', 
                'subject_type_id', 
                'subject_sample', 
                'channel', 
                'image_type', 
                'image_width', 
                'image_heigth', 
                'max', 
                'min', 
                'mean', 
                'std']

        self.jsLog.create_s_transform_statistics(self.s_transform_statistics_header)

    def _compute_s_transform(self, eeg_sample, sampling_frequency,fmin, fmax, img_shape, subject_type_id, index_subject, subject_sample, ch_names):
        channel_num = 1

        # Returns s-transform image shape (w, h, 19) (19, num channels)
        t = np.arange(eeg_sample.shape[1])/sampling_frequency

        #fmin = 0  # Hz
        #fmax = 64  # Hz
        df = 1./(t[-1]-t[0])
        fmin_samples = int(fmin/df)
        fmax_samples = int(fmax/df)

        s_transform_img = None
        for channel in eeg_sample:
            start_time = time.time()
            self.jsLog.w(f"Computing S-Transform on channel {channel_num} [{eeg_sample.shape}] [{channel.shape}]")
            
            stock = st.st(channel, fmin_samples, fmax_samples)
            stock_abs = np.abs(stock).astype(np.float32)
            self.jsLog.w(f"Computed S-Transform Original [{stock_abs.dtype}] {stock_abs.shape} | img_shape[{img_shape}] MAX: [{stock_abs.max()}] MIN: [{stock_abs.min()}] STD: [{stock_abs.std()}]")
            self.jsLog.append_s_transform_statistics_row([
                index_subject, 
                subject_type_id, 
                subject_sample, 
                ch_names[channel_num-1], 
                'original', 
                stock_abs.shape[0], 
                stock_abs.shape[1], 
                stock_abs.max(), 
                stock_abs.min(), 
                stock_abs.mean(), 
                stock_abs.std()])
            
            if img_shape:
                self.jsLog.w(f"[01] In if img_shape. img_shape [{img_shape}] | [{stock_abs.shape}]")
                im_stock_r = Image.fromarray(stock_abs)
                im_stock_r = im_stock_r.resize((img_shape[1], img_shape[0]), resample=Image.LANCZOS)
                stock_abs = np.asarray(im_stock_r)
                self.jsLog.w(f"[02] In if img_shape. img_shape [{img_shape}] | [{stock_abs.shape}]")
            
            stock_abs = np.reshape(stock_abs, (stock_abs.shape[0], stock_abs.shape[1], 1))
            
            self.jsLog.append_s_transform_statistics_row([
                index_subject, 
                subject_type_id, 
                subject_sample, 
                ch_names[channel_num-1], 
                'resized', 
                stock_abs.shape[0], 
                stock_abs.shape[1], 
                stock_abs.max(), 
                stock_abs.min(), 
                stock_abs.mean(), 
                stock_abs.std()])

            if channel_num == 1:
                s_transform_img = stock_abs.copy()
            else:
                s_transform_img = np.dstack((s_transform_img, stock_abs))
            end_time = time.time()
            self.jsLog.w(f"Computed S-Transform Resized [{stock_abs.dtype}] [{stock_abs.shape}] | img_shape[{img_shape}] MAX: [{stock_abs.max()}] MIN: [{stock_abs.min()}] STD: [{stock_abs.std()}] TIME: [{(end_time-start_time) * 10**3}] ms")
            channel_num += 1

        self.jsLog.w(f"Stransform img shape [{s_transform_img.shape}]")
        return s_transform_img

    def gen_s_transform(self, jsData, stransform_img_dirname, preprocessing_data_type, window_size, overlapping, fmin=0, fmax=64, img_shape=(256, 256), save_data = False):
                              
        self.jsLog.w(f"[gen_s_transform] | Parameters | window_size (s){window_size} | overlapping (s) {overlapping} | fmin {fmin} | fmax {fmax} | img_shape {img_shape}, | save_data {save_data}")

        data = jsData.csv_data[preprocessing_data_type]
        labels = jsData.csv_labels[preprocessing_data_type]
        samples_window = int(window_size * jsData.sampling_frequency)
        samples_overlap = int(samples_window - (samples_window * overlapping))
        num_samples = {'CONTROLS':0, 'ADHD':0}

        sample_list = []
        label_list = []
        image_list = []
        statistics_list = []
        start_time = time.time()

        # for subject_type, subject_type_id in zip(['CONTROLS', 'ADHD'], [0, 1]):
        # index_subject = 1
        num_samples=0
        for subject_data, subject_label in zip(data, labels):

            subject_type_id=subject_label[1]
            index_subject=subject_label[0]
            subject_type="CONTROLS" if subject_label[1]==0 else "ADHD"

            self.jsLog.w(f"\n-----\nComputing S-Transform on subject [{index_subject}] type [{subject_type_id}][{subject_type}] preprocessing [{preprocessing_data_type}] shape [{subject_data.shape}]\n-----\n")

            start = 0
            subject_sample = 1
            while start + samples_window < subject_data.shape[1]:
                self.jsLog.w(f"Start: {start} End: {start+samples_window} Overlap: [{samples_overlap}] Size: {subject_data.shape[1]}")
                sample = subject_data[:,start:start + samples_window]
                
                s_transform_img = self._compute_s_transform(sample, jsData.sampling_frequency, fmin, fmax, img_shape, subject_type_id, index_subject, subject_sample, jsData.montage.ch_names)
                if save_data:
                    self.jsLog.save_s_transform_imgs(
                        stransform_img_dirname,
                        preprocessing_data_type,
                        subject_type, subject_type_id, index_subject,
                        sample, subject_sample, jsData.sampling_frequency, "", 
                        jsData.montage.ch_names,
                        s_transform_img, 
                        window_size, overlapping,
                        fmin, fmax)

                if num_samples == 0: # If first iteration
                    jsData.model_data = s_transform_img.reshape(1, s_transform_img.shape[0], s_transform_img.shape[1], s_transform_img.shape[2])
                else:
                    jsData.model_data = np.vstack((jsData.model_data, s_transform_img.reshape(1, s_transform_img.shape[0], s_transform_img.shape[1], s_transform_img.shape[2])))
                    #sample_list.append(s_transform_img)
                    
                jsData.model_labels.append(subject_label)
                    
                num_samples += 1
                start += samples_overlap
                subject_sample +=1

                self.jsLog.w(f"Partial Num samples: [{num_samples}]")
                self.jsLog.w(f"Partial samples size: [{len(jsData.model_data)}][{s_transform_img.nbytes}][{(s_transform_img.nbytes * len(jsData.model_data)) / (1024*1024)}] Mb")
                self.jsLog.w(f"Partial samples size: [{len(jsData.model_data)}][{s_transform_img.nbytes}][{(s_transform_img.nbytes * len(jsData.model_data)) / (1024*1024*1024)}] Gb")
                self.jsLog.w(f"Model Data Shape: [{jsData.model_data.shape}][{jsData.model_data.nbytes/ (1024*1024*1024)}]Gb | MAX: [{jsData.model_data.max()}] | MIN: [{jsData.model_data.min()}]")
            
            gc.collect()
                        
        jsData.model_labels = np.asarray(jsData.model_labels, np.int8)
        if save_data:
            self.jsLog.save_model_labels(jsData.model_labels)

        self.jsLog.w(f"Num samples: [{num_samples}]")
        self.jsLog.w(f"Total samples size: [{len(jsData.model_data)}][{s_transform_img.nbytes}][{(s_transform_img.nbytes * len(jsData.model_data)) / (1024*1024)}] Mb")
        self.jsLog.w(f"Total samples size: [{len(sample_list)}][{s_transform_img.nbytes}][{(s_transform_img.nbytes * len(jsData.model_data)) / (1024*1024*1024)}] Gb")
        end_time = time.time()
        self.jsLog.w(f"Total Computing Time [{(end_time-start_time)/60}] min ")
        self.jsLog.w(f"---\n")

    def gen_raw_windowed_data(self, jsData, preprocessing_data_type, window_size, overlapping, standarize=True, truncate=-1, window=None):

        self.jsLog.w(f"[gen_raw_windowed_data] | Parameters | window_size (s){window_size} | overlapping (s) {overlapping} | truncate (s) {truncate}")

        samples_window = int(window_size * jsData.sampling_frequency)
        samples_overlap = int(samples_window - (samples_window * overlapping))
        max_samples = truncate*jsData.sampling_frequency if truncate > 0 else math.inf
        w=signal.get_window(window, samples_window) if window else None

        self.jsLog.w(f"Sampling Frequency [{jsData.sampling_frequency}] | window_size(samples) {samples_window} | overlapping (samples) {samples_overlap}")

        if window:
            self.jsLog.w(f"Window selected: {window}")
        else:
            self.jsLog.w(f"NOT WINDOW SELECTED")

        num_samples=0
        jsData.model_data=None
        jsData.model_labels=None
        gc.collect()

        jsData.model_labels=[]
        for subject_data, subject_label in zip(jsData.csv_data[preprocessing_data_type], jsData.csv_labels[preprocessing_data_type]):
            
            # Standarizes data
            # StandardScaler expects (n_samples, n_features) (time_steps, channels)
            # Subject data has (channels, time_steps) so we transpose two times
            std_first_channel_before = subject_data[0].std()
            if standarize: 
                subject_data = StandardScaler().fit_transform(subject_data.transpose()).transpose()

            std_first_channel_after = subject_data[0].std()
            self.jsLog.w(f"TEST STD | BEFORE[{std_first_channel_before}] AFTER[{std_first_channel_after}]")

            subject_type_id=subject_label[1]
            index_subject=subject_label[0]
            subject_type="CONTROLS" if subject_label[1]==0 else "ADHD"

            self.jsLog.w(f"SUBJECT LABEL [{subject_label}] TYPE[{subject_type_id}][{subject_type}] INDEX [{index_subject}] ")

            start = 0
            subject_sample = 1
            
            while start + samples_window < subject_data.shape[1] and start + samples_window <= max_samples:
                self.jsLog.w(f"Start: {start} End: {start+samples_window} Overlap: [{samples_overlap}] Size: {subject_data.shape[1]} - {type(subject_data)}")
                sample = subject_data[:, start:start + samples_window]
                self.jsLog.w(f"Type Sample [{type(sample)}][{sample.shape}]")

                if window:
                    sample=sample*w
                    self.jsLog.w(f"Type Window [{type(w)}][{w.shape}]")
                

                if num_samples == 0: # If first iteration
                    jsData.model_data = sample.reshape(1, sample.shape[0], sample.shape[1])
                    self.jsLog.w(f"Create first sample [{jsData.model_data.shape}]")
                else:
                    jsData.model_data = np.vstack((jsData.model_data, sample.reshape(1, sample.shape[0], sample.shape[1])))
                    
                jsData.model_labels.append(subject_label)

                num_samples += 1
                start += samples_overlap
                subject_sample +=1

                self.jsLog.w(f"Partial Num samples: [{num_samples}]")
                self.jsLog.w(f"Model Data Shape: [{jsData.model_data.shape}][{jsData.model_data.nbytes/ (1024*1024*1024)}]Gb | MAX: [{jsData.model_data.max()}] | MIN: [{jsData.model_data.min()}]")

            gc.collect()

        jsData.model_labels = np.asarray(jsData.model_labels, np.int8)

    def compute_app_entropy(self, sample):
        return feat.compute_app_entropy(sample)
    
    def compute_samp_entropy(self, sample):
        return feat.compute_samp_entropy(sample)
    
    def compute_spect_entropy(self, sample, sfreq):
        return feat.compute_spect_entropy(sfreq, sample)
    
    def compute_higuchi_fd(self, sample):
        return feat.compute_higuchi_fd(sample)
    
    def compute_rms(self, sample):
        return feat.compute_rms(sample)