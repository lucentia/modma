# Copyright (c) [2022] [Javier Sanchis; Sandra Ponsoda; XSB DISSENY I MULTIMEDIA, S.L.; LUCENTIA Lab]

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# V2.0 Adding functionality for raw data

import os
import sys
import json
import mne
import time
import re
import math

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.utils import shuffle
from JSLib.JSPreprocessingAdhd import JSPreprocessingAdhd
from JSLib.JSLog import JSLog

class JSDataAdhd:

    def __init__(self, name, config_file="configuration.json", sampling_frequency=128, int_seed=1234):
        

        # Initialize numpy random state
        self.seed=int_seed
        self.rnd = np.random.default_rng(seed=self.seed)
        self.config_file=config_file

        self.jsLog = JSLog(f"JSDataADHD_Log_{name}", config_file=self.config_file)
        self.dict_statistics = {}
        self.statistics = {}
        self.data = {}
        self.labels = {}
        self.int_seed=int_seed

        

        # Stores data and labels from csv dir
        # labels = (subject_id, type)
        self.csv_data={}
        self.csv_labels={}

        #self.original_data = np.empty(0)
        #self.original_labels = np.empty(0)

        #self.filtered_data = np.empty(0)
        #self.filtered_labels = np.empty(0)

        #self.asr_data = np.empty(0)
        #self.asr_labels = np.empty(0)

        # Model data contains the data ready to pass to the model.
        self.model_data = None
        self.model_labels = []
        self.model_statistics = []

        self.dir_path = ""
        self.dir_data = ""

        self.sampling_frequency=sampling_frequency

        self.montage = None
        self.eeg_info = None

        self.__init_paths()

        self._load_montage()
        self._create_eeg_info()

        # self._load_data()

        # self.load_s_transform_data('preprocessed_asr')

    def _add_statistics(self, subject, data_type, type, type_id, np_data):
        
        num_samples = np_data.shape[1]
        
        if data_type == 'original':
            self.dict_statistics[subject + type] = [subject, type_id, type, num_samples, np_data.size, np_data.itemsize, np_data.min(), np_data.max(), np_data.mean(), np_data.std()]
        if data_type == 'preprocessed_filter':
            self.dict_statistics[subject + type].append(num_samples)
            self.dict_statistics[subject + type].append(np_data.size)
            self.dict_statistics[subject + type].append(np_data.itemsize)
            self.dict_statistics[subject + type].append(np_data.min())
            self.dict_statistics[subject + type].append(np_data.max())
            self.dict_statistics[subject + type].append(np_data.mean())
            self.dict_statistics[subject + type].append(np_data.std())
        if data_type == 'preprocessed_asr_04':
            self.dict_statistics[subject + type].append(num_samples)
            self.dict_statistics[subject + type].append(np_data.size)
            self.dict_statistics[subject + type].append(np_data.itemsize)
            self.dict_statistics[subject + type].append(np_data.min())
            self.dict_statistics[subject + type].append(np_data.max())
            self.dict_statistics[subject + type].append(np_data.mean())
            self.dict_statistics[subject + type].append(np_data.std())

    def _save_statistics(self):
        for clave, valor in self.dict_statistics.items():
            print(f"[{clave}] - [{type(valor)}]{valor}")

        header = ['id_subject', 'id_type', 'type', 'original_num_samples', 'original_array_size', 'original_data_size', 'original_min', 'original_max', 'original_mean', 'original_std', 'filtered_num_samples', 'filtered_array_size', 'filtered_data_size', 'filtered_min', 'filtered_max', 'filtered_mean', 'filtered_std',
        'asr_num_samples', 'asr_array_size', 'asr_data_size', 'asr_min', 'asr_max', 'asr_mean', 'asr_std']
        self.jsLog.save_data_statistics(self.dict_statistics, header)
        
    
    def __init_paths(self):
        
        configuration_path = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            '..',
            '..',
            self.config_file)
        
        with open(configuration_path) as f:
            config_data = json.load(f)

        self.dir_path = os.path.dirname(os.path.realpath(__file__))
        self.dir_data = os.path.join(config_data['base_data_path'])

    def _load_montage(self):
        montage_file_name = os.path.join(
            self.dir_data, 'montage', 'Standard-10-20-Cap19new.loc')
        print("LOADING MONTAGE [{}]".format(montage_file_name))
        
        self.montage = mne.channels.read_custom_montage(
            fname=montage_file_name,
            coord_frame='head')

        print(self.montage.ch_names)
    
    def _create_eeg_info(self):

        self.eeg_info = mne.create_info(
            ch_names=self.montage.ch_names,
            ch_types=['eeg'] * len(self.montage.ch_names),
            sfreq=self.sampling_frequency)
        
        self.eeg_info.set_montage(self.montage)

    def _plot_montage(self):
        
        if self.montage==None:
            self._load_montage()

        self.montage.plot()
        fig = self.montage.plot(kind='3d', show=False)  # 3D
        fig = fig.gca().view_init(azim=70, elev=15)  # set view angle for tutorial

        plt.show()

    def plot_subject(self, preprocessing_data_type, id_subject, type_subject):
        subject_data=self.get_subject_data(preprocessing_data_type, id_subject, type_subject)
        print(type(subject_data))
        print(subject_data.shape)
    
        simulated_raw = mne.io.RawArray(subject_data, self.eeg_info)
        #simulated_raw.plot(show_scrollbars=True, show_scalebars=True)
    
        #plt.savefig("xxx.png")
        #plt.show()


        simulated_epochs=mne.make_fixed_length_epochs(
            simulated_raw,
            duration=4.0, 
            preload=True, 
            reject_by_annotation=True, 
            proj=True, 
            overlap=0.0, 
            id=1, 
            verbose='INFO')

        simulated_epochs.average().detrend().plot_joint()

        np_data=simulated_epochs.get_data()

        print(type(np_data))
        print(np_data.shape)

    def save_stats_data_type(self, preprocessing_data_type):
        self.load_data_type(preprocessing_data_type)
        print(len(self.csv_data[preprocessing_data_type]))
        i=1
        stats=[]
        jsPre=JSPreprocessingAdhd(f"Stats_{preprocessing_data_type}")
        for sample, label in zip(
            self.csv_data[preprocessing_data_type], 
            self.csv_labels[preprocessing_data_type]):

            app_entropy=jsPre.compute_app_entropy(sample)
            samp_entropy=jsPre.compute_samp_entropy(sample)
            spec_entropy=jsPre.compute_spect_entropy(sample, self.sampling_frequency)
            higuchi_fd=jsPre.compute_higuchi_fd(sample)
            rms=jsPre.compute_rms(sample)

            print(f"SE {spec_entropy}")
            print(f'[{i:03}] - [{sample.shape[1]/128}]s [{sample.shape[1]}] [{label[0]}][{label[1]}]')
            i+=1
            stats.append([label[0], label[1], sample.shape[1], sample.shape[1]/128, 
                          sample.max(), sample.min(), sample.std(), sample.mean(),
                          app_entropy.max(), app_entropy.min(), app_entropy.mean(),
                          samp_entropy.max(), samp_entropy.min(), samp_entropy.mean(),
                          spec_entropy.max(), spec_entropy.min(), spec_entropy.mean(), 
                          higuchi_fd.max(), higuchi_fd.min(), higuchi_fd.mean(), 
                          rms.max(), rms.min(), rms.mean()]
                          )

        stats_ds=pd.DataFrame(stats, columns =['subject', 'type', 'samples', 'seconds', 
                                               'max', 'min', 'std', 'mean', 
                                               'ae_max', 'ae_min', 'ae_mean',
                                               'samp_e_max', 'samp_e_min', 'samp_e_mean', 
                                               'spec_max', 'spec_min', 'spec_mean',
                                               'higuchi_fd_max', 'higuchi_fd_min', 'higuchi_fd_mean', 
                                               'rms_max', 'rms_min', 'rms_mean'])

        print(stats_ds.sort_values(by=['samples'], ascending=True).head(n=10))
        
        stats_ds.to_csv(
            os.path.join(self.dir_data,f'data_statistics_by_subject_{preprocessing_data_type}.csv')
        )

        

    # Loads data from dir=preproceesing_data_type. Stored in self.csv_data[preprocessing_data_type]. Shape (n_channels, time_steps)
    def load_data_type(self, preprocessing_data_type):

        if preprocessing_data_type in self.csv_data and len(self.csv_data[preprocessing_data_type])==121:
            self.jsLog.w(f"Data [{preprocessing_data_type}] already loaded")
            return
        else:
            self.jsLog.w(f"Data [{preprocessing_data_type}] not loaded")

        data_lengths = []
        np_labels_list = []
        
        for subject_type, subject_type_id in zip(['CONTROLS', 'ADHD'], [0, 1]):
            
            data_path = os.path.join(self.dir_data, preprocessing_data_type, subject_type)
            files = sorted(os.listdir(data_path))
            np_data_list = []

            for file in files:
                
                m=re.match(r"^(\d{2}).(csv)$", file)

                if m:

                    id_subject=int(m.group(1))
                    file_name = os.path.join(data_path, file)
                    self.jsLog.w(f"LOADING [{id_subject:02d}][{file_name}]", show=True)
                    
                    ds = pd.read_csv(file_name, sep='\t')

                    if not preprocessing_data_type in self.csv_data:
                        self.csv_data[preprocessing_data_type] = []

                    if not preprocessing_data_type in self.csv_labels:
                        self.csv_labels[preprocessing_data_type] = []

                    #np_data = ds.drop(columns=['Time']).to_numpy().transpose().astype(np.float32)
                    #if (np_data.shape[1] > 8000):
                    self.csv_data[preprocessing_data_type].append(ds.drop(columns=['Time']).to_numpy().transpose().astype(np.float32))
                    self.csv_labels[preprocessing_data_type].append((id_subject,subject_type_id))
                    #else:
                    #    self.jsLog.w(f"DISCARDING [{id_subject:02d}][{file_name}] low data [{np_data.shape[1]}]", show=True)
                    #data_lengths.append(np_data.shape[1])
                    
                    #print(f"\t SAMPLES: [{np_data.shape[1]}] - SHAPE [{np_data.shape}]")
                    #print("[{}] - [{}] - [{}]".format(file[0:2], subject_type_id, np_data.shape[1]))
                    #self._add_statistics(file[0:2], preprocessing_data_type, subject_type, subject_type_id, np_data)
        
        
        # self.statistics[preprocessing_data_type] = {"lengts":data_lengths, "max_length":max(data_lengths), "min_length" :min(data_lengths)}
        # print("\t {}[{}] - MAX: [{}], MIN: [{}]".format(preprocessing_data_type, len(data_lengths), max(data_lengths), min(data_lengths)))
        self.jsLog.w(f"Total Len DATA:{len(self.csv_data[preprocessing_data_type])} - LABELS:{len(self.csv_labels[preprocessing_data_type])}")
        
    def _load_data(self):
        '''Loads the csv EEG Data files into a numpy array

        Parameters
        ----------
        data_type : string
            String describing the data type. This values must be 'original', 
            'preprocessed_asr' or 'preprocessed_filter'
        '''

        

        for preprocessing_data_type in ['original', 'preprocessed_filter', 'preprocessed_asr_04']:
            
            data_lengths = []
            
            np_labels_list = []
            

            for subject_type, subject_type_id in zip(['CONTROLS', 'ADHD'], [0, 1]):
                
                original_data_path = os.path.join(self.dir_data, preprocessing_data_type, subject_type)
                files = sorted(os.listdir(original_data_path))
                np_data_list = []

                for file in files:

                    m=re.match(r"^(\d{2}).(csv)$", file)

                    if m:
                        id_subject=int(m.group(1))
                        file_name = os.path.join(original_data_path, file)
                        self.jsLog.w(f"LOADING [{id_subject:02d}][{file_name}]", show=True)
                        
                        ds = pd.read_csv(file_name, sep='\t')

                        # np_data with shape (channels, time_steps)
                        np_data = ds.drop(columns=['Time']).to_numpy().transpose().astype(np.float32)

                        np_data_list.append(np_data)
                        #np_labels_list.append(subject_type_id)
                        data_lengths.append(np_data.shape[1])
                        
                        #print(f"\t SAMPLES: [{np_data.shape[1]}] - SHAPE [{np_data.shape}]")
                        #print("[{}] - [{}] - [{}]".format(file[0:2], subject_type_id, np_data.shape[1]))
                        self._add_statistics(file[0:2], preprocessing_data_type, subject_type, subject_type_id, np_data)

                print(f"SAVING [{preprocessing_data_type}] [{subject_type}]", end = '')
            
                if not preprocessing_data_type in self.data:
                    self.data[preprocessing_data_type] = {}

                self.data[preprocessing_data_type][subject_type] = np_data_list.copy()
            
            self.labels[preprocessing_data_type] = np_labels_list.copy()
            self.statistics[preprocessing_data_type] = {"lengts":data_lengths, "max_length":max(data_lengths), "min_length" :min(data_lengths)}
            self.jsLog.w("\t {}[{}] - MAX: [{}], MIN: [{}]".format(preprocessing_data_type, len(data_lengths), max(data_lengths), min(data_lengths)), show=True)

        # print(self.data.keys())

        # self._save_statistics()

        for key in self.data.keys():
            print("{} - Length: [{}]".format(key, len(self.data[key])))
            print("Data Subject {} CONTROL 20 P8 \n {} \n Length [{}]".format(key, self.data[key]['CONTROLS'][20][15,:], self.data[key]['CONTROLS'][20][15,:].shape))
            print(self.montage.ch_names)

    def load_data(self):

        self._load_montage()
        self._create_eeg_info()
        
        self._load_data()

    
    
    def get_subject_data(self, preprocessing_data_type, id_subject, subject_type):
        
        print(f"Returnning {preprocessing_data_type} {subject_type} {id_subject}")
    
        return self.csv_data[preprocessing_data_type][
            self.csv_labels[preprocessing_data_type].index((id_subject, subject_type))]


    # TODO Now window_size and overlapping are floats
    def _get_npy_file_path(self, transform_name:str, preprocessing_data_type:str, window_size:int, overlapping:int, fmin:int, fmax:int, img_shape:tuple, truncate:int):

        w = 0
        h = 0
        if img_shape:
            h = img_shape[0]
            w = img_shape[1]
        
        if truncate > 0:
            s_transform_file_name = f"model_data_{transform_name}_{window_size}_{overlapping}_{fmin}_{fmax}_{h}_{w}_{preprocessing_data_type}_{truncate}_s.npy"
        else:
            s_transform_file_name = f"model_data_{transform_name}_{window_size}_{overlapping}_{fmin}_{fmax}_{h}_{w}_{preprocessing_data_type}.npy"
        
        return os.path.join(self.dir_data, s_transform_file_name), s_transform_file_name[:-4]

    def load_s_transform_data(self, preprocessing_data_type, window_size, overlapping, fmin=0, fmax=64, img_shape=(256, 256), save_data = True, truncate:int=-1):
        
        s_transform_file_path, stransform_img_dirname = self._get_npy_file_path('stransform', preprocessing_data_type, window_size, overlapping, fmin, fmax, img_shape, truncate)

        #if exists precomputed s_transform this is loaded from disk
        if os.path.exists(s_transform_file_path):
            start_time = time.time()
            self.jsLog.w(f"Loading s_transform from file [{s_transform_file_path}]")
            with open(s_transform_file_path, 'rb') as f:
                self.model_data = np.load(f)
                self.model_labels = np.load(f)
                self.model_labels = self.model_labels.astype(np.float32)
            
            end_time = time.time()
            self.jsLog.w(f"Loaded s_transform from file [{s_transform_file_path}]") 
            self.jsLog.w(f"s-transform data [{self.model_data.shape}][{self.model_data.dtype}][{self.model_data.nbytes / (1024**3) }]Gb | [{self.model_labels.shape}][{self.model_labels.dtype}][{self.model_labels.nbytes / (1024**3) }]Gb || [{(end_time-start_time)/60}]min")
            self.jsLog.w(f"Model Data: [{self.model_data.dtype}][{self.model_data.shape}] MIN[{self.model_data.min()}] MAX[{self.model_data.max()}] MEAN[{self.model_data.mean()}]")
            self.jsLog.w(f"Model Labels: [{self.model_labels.dtype}][{self.model_labels.shape}] MIN[{self.model_labels.min()}] MAX[{self.model_labels.max()}] MEAN[{self.model_labels.mean()}]")

        else:
            self.jsLog.w(f"[{s_transform_file_path}] not found. Computing s-transform")
            start_time = time.time()
            self.load_data_type(preprocessing_data_type)
            img_h=0
            img_w=0
            if img_shape:
                img_h=img_shape[0]
                img_w=img_shape[1]

            jsPreprocessingAdhd = JSPreprocessingAdhd(f"{preprocessing_data_type}_{img_h}_{img_w}")
            jsPreprocessingAdhd.gen_s_transform(self, stransform_img_dirname, preprocessing_data_type, window_size, overlapping, fmin=fmin, fmax=fmax, img_shape=img_shape, save_data=save_data)

            # saving computed data to file
            
            with open(s_transform_file_path, 'wb') as f:
                self.jsLog.w(f"Saving data to [{s_transform_file_path}]")
                np.save(f, self.model_data)
                np.save(f, self.model_labels)

            end_time = time.time()
            self.jsLog.w(f"Saved s_transform file [{s_transform_file_path}] [{self.model_data.shape}][{self.model_data.dtype}][{self.model_data.nbytes / (1024**3) }]Gb | [{self.model_labels.shape}][{self.model_labels.dtype}][{self.model_labels.nbytes / (1024**3) }]Gb || [{(end_time-start_time)/60}]min")

    def _get_npy_file_path_raw(self, preprocessing_data_type:str, window_size:float, overlapping:float, standarize:bool, truncate:int, window:str):

        window_size_str=str(window_size).replace('.', '_')
        overlapping_str=str(overlapping).replace('.', '_')

        if truncate > 0:
            str_truncate=str(truncate)
        else:
            str_truncate="0"

        str_truncate=str(truncate) if truncate > 0 else "0"
        str_window=window if window else "None"
        
        raw_file_name = f"model_data_raw_{window_size_str}__{overlapping_str}__std_{standarize}__{preprocessing_data_type}__t_{str_truncate}s__w_{str_window}.npy"
            
        return os.path.join(self.dir_data, raw_file_name), raw_file_name[:-4]


    # TO-DO all must start load_model_data_{type} where type is s_transform, raw, spectrogram, wavelet .....
    # load_data_* loads from disk
    # load_model_data_* adapts loaded data from disk to a DL model and is stored in self.model_data, self.model_labels
    def load_model_data_raw(self, preprocessing_data_type:str, window_size:float=2.0, overlapping:float=0.5, standarize:bool=True, truncate:int=-1, window:str=None, outliers:str=None):

        data_file_path, data_dirname = self._get_npy_file_path_raw(preprocessing_data_type, window_size, overlapping, standarize, truncate, window)

        #if exists precomputed raw this is loaded from disk
        if os.path.exists(data_file_path):
            start_time = time.time()
            self.jsLog.w(f"Loading raw from file [{data_file_path}]", True)
            with open(data_file_path, 'rb') as f:
                self.model_data = np.load(f)
                self.model_labels = np.load(f)
            
            end_time = time.time()
            
            self.jsLog.w(f"Loaded data from file [{data_file_path}] in [{end_time-start_time}]s") 
            self.jsLog.w(f"Model Data: [{self.model_data.dtype}][{self.model_data.shape}] MIN[{self.model_data.min()}] MAX[{self.model_data.max()}] MEAN[{self.model_data.mean()}]")
            self.jsLog.w(f"Model Labels: [{self.model_labels.dtype}][{self.model_labels.shape}] MIN[{self.model_labels.min()}] MAX[{self.model_labels.max()}] MEAN[{self.model_labels.mean()}]")
        
        else:
            self.jsLog.w(f"[{data_file_path}] not found. Windowing raw data", True)
            start_time = time.time()
            self.load_data_type(preprocessing_data_type)
            jsPreprocessingAdhd = JSPreprocessingAdhd(f"{preprocessing_data_type}_raw")
            jsPreprocessingAdhd.gen_raw_windowed_data(self, preprocessing_data_type, window_size, overlapping, standarize=standarize, truncate=truncate, window=window)
            
            with open(data_file_path, 'wb') as f:
                self.jsLog.w(f"Saving data to [{data_file_path}]")
                np.save(f, self.model_data)
                np.save(f, self.model_labels)

            end_time = time.time()
            self.jsLog.w(f"Saved raw windowed file [{data_file_path}] [{self.model_data.shape}][{self.model_data.dtype}][{self.model_data.nbytes / (1024**3) }]Gb | [{self.model_labels.shape}][{self.model_labels.dtype}][{self.model_labels.nbytes / (1024**3) }]Gb || [{(end_time-start_time)/60}]min")

        self._remove_outliers(window_size, overlapping, outliers)
    
    def _remove_outliers(self, window_size:float=2.0, overlapping:float=0.5, outliers:str=None):

        
        list_bad_channels=None
        self.jsLog.w(f"[OUTLIERS_] - [{outliers}] - [{type(outliers)}]", True)
        if outliers != None and outliers != "None":
            min_seconds=60 # default min value
            try:
                outliers_metadata=json.loads(outliers.replace("'", "\""))
                min_seconds=int(outliers_metadata["m"])
                list_bad_channels=outliers_metadata["bad"]
                self.jsLog.w(f"[OUTLIERS META DATA] - [{min_seconds}] - [{list_bad_channels}]", True)
            except Exception as err:
                self.jsLog.w(f"[ERROR] - [{err}]", True)    

            window_len=window_size*self.sampling_frequency
            window_step=window_size*(1-overlapping)*self.sampling_frequency
            min_num_samples=math.floor(((min_seconds*self.sampling_frequency)-window_len)/window_step)+1
            self.jsLog.w(f"MIN SAMPLES = [{min_seconds*self.sampling_frequency}][{min_num_samples}] - MIN SECONDS [{min_seconds}] - WINDOW STEP [{window_step}] - SF [{self.sampling_frequency}] - WINDOWS LEN [{window_len}]", True)

            # Get num windows samples

            self.jsLog.w(f"Labels [{type(self.model_labels)}] - SHAPE [{self.model_labels.shape}]", True)
            data_set_labels=pd.DataFrame(self.model_labels, columns=['subject', 'type'])
            print(data_set_labels.head())
            data_grouped=data_set_labels.groupby(['subject', 'type']).value_counts()
            print(data_grouped)
            data_grouped=data_grouped.reset_index(level=[0,1])
            data_grouped.columns=['subject', 'type', 'num_frames']
            print(data_grouped.sort_values(by=['num_frames'], ascending=True))
            print(data_grouped['num_frames'].sum())
            
            data_filtered=data_grouped[data_grouped['num_frames']<min_num_samples]

            print(data_filtered)

            for index, row in data_filtered.iterrows():
                print(f"Shapes before Remove {self.model_labels.shape}, {self.model_data.shape}")
                print(f"({row['subject']}, {row['type']})")
                remove_mask=np.invert(np.isin(self.model_labels[:,0], row['subject']) &  np.isin(self.model_labels[:,1], row['type']))
                self.model_labels=self.model_labels[remove_mask]
                self.model_data=self.model_data[remove_mask]
                print(f"Shapes after Remove {self.model_labels.shape}, {self.model_data.shape}")

        if list_bad_channels != None:
            for bad_channels_label in list_bad_channels:
                print(f"Shapes before Remove Bad Channels {self.model_labels.shape}, {self.model_data.shape}")
                print(f"({bad_channels_label[0]}, {bad_channels_label[1]})")
                remove_mask=np.invert(np.isin(self.model_labels[:,0], bad_channels_label[0]) &  np.isin(self.model_labels[:,1], bad_channels_label[1]))
                self.model_labels=self.model_labels[remove_mask]
                self.model_data=self.model_data[remove_mask]
                print(f"Shapes after Remove Bad Channels {self.model_labels.shape}, {self.model_data.shape}")

    def get_train_model_data_by_subject_type(self, index_subject, type_subject):

        mask_subject_type=np.isin(self.model_labels[:,0], [index_subject]) & np.isin(self.model_labels[:,1], [type_subject])

        X_test_subject = self.model_data[mask_subject_type].copy()
        y_test_subject = self.model_labels[:,1][mask_subject_type]

        return X_test_subject, y_test_subject

    def get_train_test_val_model_data(self, train_index, test_index, normalize=False, num_validation_subjects=4):

        np.random.seed(self.seed)

        val_index = np.random.choice(train_index, num_validation_subjects, False)
        train_index = np.delete(train_index, np.isin(train_index, val_index))

        X_train = self.model_data[np.isin(self.model_labels[:,0], train_index)].copy()
        y_train = self.model_labels[:,1][np.isin(self.model_labels[:,0], train_index)].copy()

        X_val = self.model_data[np.isin(self.model_labels[:,0], val_index)].copy()
        y_val = self.model_labels[:,1][np.isin(self.model_labels[:,0], val_index)].copy()

        X_test = self.model_data[np.isin(self.model_labels[:,0], test_index)].copy()
        y_test = self.model_labels[:,1][np.isin(self.model_labels[:,0], test_index)].copy()

        if normalize:
            min_X = X_train.min()
            max_X = X_train.max()
            X_train = (X_train - min_X) / (max_X - min_X)
            X_val = (X_val - min_X) / (max_X - min_X)
            X_test = (X_test - min_X) / (max_X - min_X)
            self.jsLog.w(f"Data Normalized min_X[{min_X}] max_X[{max_X}]")

        X_train, y_train = shuffle(X_train, y_train)
        X_val, y_val = shuffle(X_val, y_val)
        X_test, y_test = shuffle(X_test, y_test)

        return X_train, y_train, X_val, y_val, X_test, y_test, train_index, val_index, test_index
        