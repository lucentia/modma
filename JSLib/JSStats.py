import argparse
import json
import sys

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from os import path
from os import mkdir


class JSStats():
    def __init__(self):
        
        configuration_path = path.join(
            path.dirname(path.realpath(__file__)),
            '..',
            '..',
            'configuration_local.json'
        )

        with open(configuration_path) as f:
            config_data = json.load(f)

        print(config_data['base_log_path'])
        print(config_data['base_data_path'])

        self.log_path=config_data['base_log_path']
        self.data_path=config_data['base_data_path']

        self.read_data_ok=False

        self.g_res=[]

    def _get_data(self, name, type='r'):

        if type =='r':
            filename='results.csv'
        elif type=='s':
            filename='results_by_subject.csv'
        
        csv_results_path = path.join(
            self.log_path,
            name,
            'results',
            filename
        )

        print(csv_results_path)
        ret_data=None
        self.read_data_ok=False
        if path.exists(csv_results_path):
            self.read_data_ok=True
            ret_data=pd.read_csv(csv_results_path)

        return ret_data

    def show_results(self, name):
        data = self._get_data(name, type='r')

        print(data[['ix_iteration', 'binary_accuracy', 'precision', 'recall', 'comp_specificity', 'comp_f1']].groupby('ix_iteration').mean())

        print(data[['ix_iteration', 'binary_accuracy', 'precision', 'recall', 'comp_specificity', 'comp_f1']].mean())

    def plot_results(self, name):

        data = self._get_data(name, type='r')

        for iter in data.ix_iteration.unique():
            result_data_iter = data.loc[(data['ix_iteration'] == iter)]

            ax = result_data_iter.plot.bar(x='ix_fold', y=['binary_accuracy', 'precision', 'recall', 'comp_f1'])

            self._savefig(ax.figure, name, f"results_{iter}.png")
        
    def _savefig(self, fig, experiment_name, filename):

        plot_path = path.join(
            self.log_path,
            experiment_name,
            'plots'
        )

        if not path.exists(plot_path):
            mkdir(plot_path)

        fig.savefig(path.join(plot_path, filename))

    def plot_history(self, name):
        csv_history_path = path.join(
            self.log_path,
            name,
            'history_log.csv'
        ) 

        print(csv_history_path)

        data = pd.read_csv(csv_history_path)
        history_data = data[['ix_iteration', 'ix_fold', 'history_step', 'loss', 'val_loss', "binary_accuracy", "val_binary_accuracy"]]

        num_folds = data.ix_fold.nunique()

        for iter in data.ix_iteration.unique():

            fig, axes = plt.subplots(nrows=num_folds, ncols=2)

            fig.suptitle(f"History - {name} - ix_iteration [{iter}]", fontsize=16)
            fig.set_figwidth(7)
            fig.set_figheight(11)

            for ix_fold in data.ix_fold.unique():
                history_data_fold = history_data.loc[(
                    (history_data['ix_iteration'] == iter) &
                    (history_data['ix_fold'] == ix_fold)
                )]

                axes[ix_fold,0].set_ylim(bottom=0, top=2)
                history_data_fold.plot(ax=axes[ix_fold,0], x="history_step", y=["loss", "val_loss"], kind="line", legend=None)

                axes[ix_fold,1].set_ylim(bottom=0.5, top=1.1)
                history_data_fold.plot(ax=axes[ix_fold,1], x="history_step", y=["binary_accuracy", "val_binary_accuracy"], kind="line", legend=None)

                self._savefig(fig, name, f"History_{name}_Iteration_{iter}.png")

    def show_total_time(self, name):
        data = self._get_data(name, type='r')
        print(data['time'].sum()/(60*60))

    def evaluate_by_subject(self, name, threshold=0.6):
        data = self._get_data(name, type='s')
        
        if not self.read_data_ok:
            print(f"{name} does not exists.")
        
        else:

            
            data_r=data[['ix_iteration','ix_fold','subject_step','index_subject','type_subject','value']].copy()
            # print(data_r.head())

            data_r['value_computed']=np.where(data_r['value'] > threshold, 1, 0)
            
            data_r['result']=np.where(data_r['type_subject']==data_r['value_computed'], 1, 0)
            # print(data_r.head())

            csv_results_tp_path_out=path.join(
                self.log_path,
                name,
                'results',
                'results_by_subject_tp_tn_fp_fn.csv'
            )
            data_r.to_csv(csv_results_tp_path_out)
            
            for index_iteration in range(4):
                data_filtered=data_r[data_r['ix_iteration']==index_iteration].copy()
                
                data_grouped_r=data_filtered.groupby(['index_subject','type_subject']).agg(
                    total_samples=('index_subject', 'count'), 
                    total_ok=('result', 'sum'),
                    mean_value=('value', 'mean')).copy()
                
                data_grouped=data_grouped_r.reset_index().copy()

                # print(dgi.head())

                data_grouped['num']=np.where((data_grouped['total_samples']//2) > data_grouped['total_ok'], 0, 1)
                data_grouped['level']=data_grouped['total_ok'] / data_grouped['total_samples']
                data_grouped['need']=data_grouped['total_samples']/2 - data_grouped['total_ok'] 
                data_grouped['true_positives']=np.where((data_grouped['type_subject']==1) & (data_grouped['num']==1), 1, 0)
                data_grouped['true_negatives']=np.where((data_grouped['type_subject']==0) & (data_grouped['num']==1), 1, 0)

                data_grouped['false_positives']=np.where((data_grouped['type_subject']==0) & (data_grouped['num']==0), 1, 0)
                data_grouped['false_negatives']=np.where((data_grouped['type_subject']==1) & (data_grouped['num']==0), 1, 0)

                # print(data_grouped.head())
                str_t=f'{threshold}'.replace('.', '_')
                csv_results_path_out=path.join(
                    self.log_path,
                    name,
                    'results',
                    f'results_by_subject_grouped_{index_iteration:02}_{str_t}.csv'
                )

                data_grouped.to_csv(csv_results_path_out)
                
                tp=data_grouped['true_positives'].sum()
                tn=data_grouped['true_negatives'].sum()
                fp=data_grouped['false_positives'].sum()
                fn=data_grouped['false_negatives'].sum()
                
                accuracy=( tp + tn )/(tp+tn+fp+fn)
                precision=tp/(tp+fp)
                recall=tp/(tp+fn)
                specificity=tn/(tn+fp)
                f1=(2*precision*recall)/(precision+recall)

                print(f"{name}[{index_iteration:02}]: [{threshold:4f}] [{data_grouped['num'].sum()}][{data_grouped['num'].count()}] [{data_grouped['num'].sum()/data_grouped['num'].count()}] [{accuracy}] [{precision}] [{recall}] [{specificity}] [{f1}]")
                
                data_grouped=None
                data_grouped_r=None
                data_filtered=None

                self.g_res.append([name, index_iteration, threshold, accuracy, precision, recall, specificity, f1])

            print(f"- [{len(self.g_res)}] -")

    def save_results(self, name, start, end):
    
        g_res_ds=pd.DataFrame(self.g_res, columns =['name', 'index_iteration', 'threshold', 'accuracy', 'precision', 'recall', 'specificity', 'f1'])

        print(g_res_ds.sort_values(by=['accuracy'], ascending=False).head(n=40))
        
        g_res_ds.to_csv(
            path.join(self.log_path,f'global_results_bySubject_{name}_{start}_{end}.csv')
        )




            
        
        


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog = 'ProgramName',
        description = 'What the program does',
        epilog = 'Text at the bottom of help')

    parser.add_argument('-s', '--start')
    parser.add_argument('-e', '--end')
    parser.add_argument('-m', '--model')
    args = parser.parse_args()

    jsStats = JSStats()

    print(f"Start {args.start} - End {args.end}")

    g_res=[]
    for index in range(int(args.start),int(args.end)):
        name=args.model
        experiment_name=f'{name}_{index:02}'
        #experiment_name=f'MultiDimensionalCNN_{index:02}'
        #experiment_name=f'EEGNet_{index:02}'
        jsStats.evaluate_by_subject(experiment_name, 0.6500)
        jsStats.evaluate_by_subject(experiment_name, 0.6400)
        jsStats.evaluate_by_subject(experiment_name, 0.6300)
        jsStats.evaluate_by_subject(experiment_name, 0.6200)
        jsStats.evaluate_by_subject(experiment_name, 0.6335)
        jsStats.evaluate_by_subject(experiment_name, 0.6235)
        jsStats.evaluate_by_subject(experiment_name, 0.6135)
        jsStats.evaluate_by_subject(experiment_name, 0.5500)
        jsStats.evaluate_by_subject(experiment_name, 0.5000)
        print("---")

    jsStats.save_results(name, args.start, args.end)





       