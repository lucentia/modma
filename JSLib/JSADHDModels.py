import ast
import tensorflow as tf

class MultiDimensionalCNN_Conv2D(tf.keras.layers.Layer):

    def __init__(self,p):
        
        super(MultiDimensionalCNN_ClassificationDense, self).__init__()

        self._init_params(p)

        self.block_01_exp_conv2D_time_LF=tf.keras.layers.Conv2D(128, (128,1), padding='same')
        self.block_01_exp_conv2D_time_MF=tf.keras.layers.Conv2D(128, (64,1), padding='same')
        self.block_01_exp_conv2D_time_HF=tf.keras.layers.Conv2D(128, (2,1), padding='same')

        self.block_01_exp_conv2D_mixed_LF=tf.keras.layers.Conv2D(128, (64,252), padding='same')
        self.block_01_exp_conv2D_mixed_MF=tf.keras.layers.Conv2D(128, (8,16), padding='same')
        self.block_01_exp_conv2D_mixed_HF=tf.keras.layers.Conv2D(128, (2,4), padding='same')

        self.block_01_exp_conv2D_chans_LF=tf.keras.layers.Conv2D(128, (1,504), padding='same')
        self.block_01_exp_conv2D_time_MF=tf.keras.layers.Conv2D(128, (1,63), padding='same')
        self.block_01_exp_conv2D_time_HF=tf.keras.layers.Conv2D(128, (1,3), padding='same')

    def call(self, input_tensor, training=False):

        x=self.block_01_exp_conv2D_time_LF(input_tensor)
        x=self.block_01_exp_conv2D_time_MF(x)
        x=self.block_01_exp_conv2D_time_HF(x)

        x=self.block_01_exp_conv2D_mixed_LF(x)
        x=self.block_01_exp_conv2D_mixed_MF(x)
        x=self.block_01_exp_conv2D_mixed_HF(x)

        x=self.block_01_exp_conv2D_chans_LF(x)
        x=self.block_01_exp_conv2D_chans_MF(x)
        x=self.block_01_exp_conv2D_chans_HF(x)

        return x

    def _init_params(self,p):
        pass

    
class MultiDimensionalCNN_ClassificationDense(tf.keras.layers.Layer):

    def __init__(self,p):
        
        super(MultiDimensionalCNN_ClassificationDense, self).__init__()

        self._init_params(p)

        self.block_classification_dense_01=tf.keras.layers.Dense(
            16, 
            kernel_constraint=tf.keras.constraints.max_norm(self.norm_rate)
        )
        self.drop_01=tf.keras.layers.Dropout(self.dropout_rate)

        self.block_classification_dense_02=tf.keras.layers.Dense(
            4, 
            kernel_constraint=tf.keras.constraints.max_norm(self.norm_rate)
        )
        self.drop_02=tf.keras.layers.Dropout(self.dropout_rate)

        self.block_classification_dense_03=tf.keras.layers.Dense(
            1, 
            kernel_constraint=tf.keras.constraints.max_norm(self.norm_rate)
        )
        self.drop_03=tf.keras.layers.Dropout(self.dropout_rate)
        self.norm=tf.keras.layers.BatchNormalization()
        
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def _init_params(self, p):
        self.dropout_rate=float(p.parameters["dropout_rate"])
        self.norm_rate=float(p.parameters["norm_rate"])

    def call(self, input_tensor, training=False):

        x=self.block_classification_dense_01(input_tensor)
        x=self.drop_01(x)

        x=self.block_classification_dense_02(x)
        x=self.drop_02(x)

        x=self.block_classification_dense_03(x)
        x=self.drop_03(x)

        x=self.norm(x, training=training)

        x=self.block_classification_activation(x)

        return x


class MultiDimensionalCNN(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiDimensionalCNN, self).__init__()

        self._init_params(p)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        # Block 01
        
        self.block_01_conv1D_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if self.same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    self.filters_f1*filter_multiplier, 
                    self.kernel_size//2**index_input_conv_layers, 
                    padding=self.padding,
                    use_bias=self.use_bias,
                    kernel_constraint=tf.keras.constraints.max_norm(self.norm_rate))
            )
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()
        self.block_01_activation_01=tf.keras.layers.Activation(self.activation_block_01)

        ## MultiDim Block

        ##
        self.block_01_exp_conv2D_time_LF=tf.keras.layers.SeparableConv2D(32, (64,1), padding='same', depth_multiplier=2, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate), pointwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        # Pool
        self.block_01_exp_conv2D_time_LF_pool=tf.keras.layers.MaxPooling2D(2,4)
        ##
        self.block_01_exp_conv2D_time_HF=tf.keras.layers.SeparableConv2D(32, (2,1), padding='same', depth_multiplier=2, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate), pointwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        # Pool
        self.block_01_exp_conv2D_time_HF_pool=tf.keras.layers.MaxPooling2D(2,4)

        ##
        self.block_01_exp_conv2D_mixed_LF=tf.keras.layers.SeparableConv2D(32, (8,32), padding='same', depth_multiplier=2, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate), pointwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        # Pool
        self.block_01_exp_conv2D_mixed_LF_pool=tf.keras.layers.MaxPooling2D(2,4)
        ##
        self.block_01_exp_conv2D_mixed_HF=tf.keras.layers.SeparableConv2D(32, (2,4), padding='same', depth_multiplier=2, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate), pointwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        # Pool
        self.block_01_exp_conv2D_mixed_HF_pool=tf.keras.layers.MaxPooling2D(2,4)

        ##
        self.block_01_exp_conv2D_feat_LF=tf.keras.layers.SeparableConv2D(32, (1,256), padding='same', depth_multiplier=2, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate), pointwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        # Pool
        self.block_01_exp_conv2D_feat_LF_pool=tf.keras.layers.MaxPooling2D(2,4)
        ##
        self.block_01_exp_conv2D_feat_HF=tf.keras.layers.SeparableConv2D(32, (1,3), padding='same', depth_multiplier=2, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate), pointwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        # Pool
        self.block_01_exp_conv2D_feat_HF_pool=tf.keras.layers.MaxPooling2D(2,4)

        ########

        self.block_01_norm_02=tf.keras.layers.BatchNormalization()

        self.block_01_activation_02=tf.keras.layers.Activation(self.activation_block_01)
        if self.pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling2D(self.pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling2D(self.pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(self.dropout_rate)

        # Block 02

        self.block_02_deptwise_01=tf.keras.layers.DepthwiseConv2D((1,31), padding='same', depth_multiplier=4, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        self.block_02_deptwise_01_pool=tf.keras.layers.MaxPooling2D(8,16)

        self.block_02_deptwise_02=tf.keras.layers.DepthwiseConv2D((16,1), padding='same', depth_multiplier=4, use_bias=self.use_bias, depthwise_constraint=tf.keras.constraints.max_norm(self.norm_rate))
        self.block_02_deptwise_02_pool=tf.keras.layers.MaxPooling2D(8,16)

        self.block_02_norm_01=tf.keras.layers.BatchNormalization()

        self.block_02_activation=tf.keras.layers.Activation(self.activation_block_02)
        if self.pool_type_block_01=='MAX':
            self.block_02_pool=tf.keras.layers.MaxPooling2D(self.pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling2D(self.pool_size_block_02)
        
        self.block_02_drop=tf.keras.layers.Dropout(self.dropout_rate)


        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        self.classification=MultiDimensionalCNN_ClassificationDense(p)

    def call(self, input_tensor, training=False):

        input=self.permute(input_tensor)

        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(input)
            )

        block_01=tf.concat(x_block_01_conv, -1)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_01(block_01, training=training)

        block_01=self.block_01_activation_01(block_01)

        block_01_expanded=tf.expand_dims(block_01, -1, name='Adapting_2D')

        
        x_time_LF=self.block_01_exp_conv2D_time_LF(block_01_expanded)
        x_time_LF=self.block_01_exp_conv2D_time_LF_pool(x_time_LF)

        x_time_HF=self.block_01_exp_conv2D_time_HF(block_01_expanded)
        x_time_HF=self.block_01_exp_conv2D_time_HF_pool(x_time_HF)

        x_mixed_LF=self.block_01_exp_conv2D_mixed_LF(block_01_expanded)
        x_mixed_LF=self.block_01_exp_conv2D_mixed_LF_pool(x_mixed_LF)

        x_mixed_HF=self.block_01_exp_conv2D_mixed_HF(block_01_expanded)
        x_mixed_HF=self.block_01_exp_conv2D_mixed_HF_pool(x_mixed_HF)

        x_feat_LF=self.block_01_exp_conv2D_feat_LF(block_01_expanded)
        x_feat_LF=self.block_01_exp_conv2D_feat_LF_pool(x_feat_LF)

        x_feat_HF=self.block_01_exp_conv2D_feat_HF(block_01_expanded)
        x_feat_HF=self.block_01_exp_conv2D_feat_HF_pool(x_feat_HF)

        #TO-DO rename and CONSTRAINTS
        block_01_exp_c=tf.concat(
            [
                x_time_LF, x_time_HF,
                x_mixed_LF, x_mixed_HF,
                x_feat_LF,  x_feat_HF
            ], -1)


        block_01_exp_c=self.block_01_norm_02(block_01_exp_c)
        block_01_exp_c=self.block_01_activation_02(block_01_exp_c)
        #block_01=self.block_01_pool(block_01)
        block_01_exp_c=self.block_01_drop(block_01_exp_c)


        block_02_depth_01=self.block_02_deptwise_01(block_01_exp_c)
        block_02_depth_01_pool=self.block_02_deptwise_01_pool(block_02_depth_01)

        block_02_depth_02=self.block_02_deptwise_02(block_01_exp_c)
        block_02_depth_02_pool=self.block_02_deptwise_02_pool(block_02_depth_02)

        block_02=tf.concat(
            [
                block_02_depth_01_pool,
                block_02_depth_02_pool
            ], -1)

        block_02=self.block_02_norm_01(block_02)
        block_02=self.block_02_activation(block_02)
        block_02=self.block_02_drop(block_02)
        
        block_02=self.block_02_flat(block_02)
        
        output=self.classification(block_02)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
    def _init_params(self, p):

        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        self.filters_f1=int(p.parameters["filters_f1"])
        self.same_number_filters=p.parameters["same_number_filters"]=='True'
        self.kernel_size=int(p.parameters["kernel_size"])
        self.activation_block_01=p.parameters["activation_block_01"]
        self.pool_type_block_01=p.parameters["pool_type_block_01"]
        self.pool_size_block_01=int(p.parameters["pool_size_block_01"])
        self.padding=p.parameters["padding"]
        self.depth_multiplier=int(p.parameters["depth_multiplier"])
        self.depth_max_norm=float(p.parameters["depth_max_norm"])
        self.use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        self.filters_f2=int(p.parameters["filters_f2"])
        self.kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        self.activation_block_02=p.parameters["activation_block_02"]
        self.pool_type_block_02=p.parameters["pool_type_block_02"]
        self.pool_size_block_02=int(p.parameters["pool_size_block_02"])
        self.dropout_rate=float(p.parameters["dropout_rate"])
        self.norm_rate=float(p.parameters["norm_rate"])
        self.apply_batch_norm=p.parameters["apply_batch_norm"]=='True'


# MultiScaleCNN_ParallelBlock
class Classification_DenseParallel_Block(tf.keras.layers.Layer):
    def __init__(self,units:int,norm_rate:float,activation:str):

        super(Classification_DenseParallel_Block, self).__init__()

        self.block_classification_dense=tf.keras.layers.Dense(
            units, 
            kernel_constraint=tf.keras.constraints.max_norm(norm_rate))
        self.block_activation=tf.keras.layers.Activation(activation)

    def call(self, input_tensor, training=False):

        x=self.block_classification_dense(input_tensor)
        x=self.block_activation(x)

        return x

class Classification_DenseParallel(tf.keras.layers.Layer):
    def __init__(self,p):

        self._init_params(p)

        super(Classification_DenseParallel, self).__init__()

        self.class_block_01=Classification_DenseParallel_Block(2, self.norm_rate, 'gelu')
        self.class_block_02=Classification_DenseParallel_Block(2, self.norm_rate, 'tanh')
        self.class_block_03=Classification_DenseParallel_Block(4, self.norm_rate, 'gelu')
        self.class_block_04=Classification_DenseParallel_Block(4, self.norm_rate, 'tanh')
        self.class_block_05=Classification_DenseParallel_Block(8, self.norm_rate, 'gelu')
        self.class_block_06=Classification_DenseParallel_Block(8, self.norm_rate, 'tanh')

        self.concat=tf.keras.layers.Concatenate()
        self.drop01=tf.keras.layers.Dropout(self.dropout_rate)

        self.classification_concat=Classification_DenseParallel_Block(32, self.norm_rate, 'gelu')
        self.drop02=tf.keras.layers.Dropout(self.dropout_rate)

        self.classification_output=Classification_Dense(p)

    def call(self, input_tensor, training=False):

        x1=self.class_block_01(input_tensor)
        x2=self.class_block_02(input_tensor)
        x3=self.class_block_03(input_tensor)
        x4=self.class_block_04(input_tensor)
        x5=self.class_block_06(input_tensor)
        x6=self.class_block_06(input_tensor)

        x=self.concat([x1, x2, x3, x4, x5, x6])
        x=self.drop01(x)

        x=self.classification_concat(x)
        x=self.drop02(x)

        output=self.classification_output(x)

        return output

    def _init_params(self, p):

        self.p=p
        self.norm_rate=float(p.parameters["norm_rate"])
        self.dropout_rate=float(p.parameters["dropout_rate"])
        
    
class Classification_Dense(tf.keras.layers.Layer):
    def __init__(self,p):

        super(Classification_Dense, self).__init__()

        self._init_params(p)

        self.block_classification_dense=tf.keras.layers.Dense(
            1, 
            kernel_constraint = tf.keras.constraints.max_norm(self.norm_rate))
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        classification=self.block_classification_dense(input_tensor)
        output=self.block_classification_activation(classification)

        return output

    def _init_params(self, p):

        self.p=p
        self.norm_rate=float(p.parameters["norm_rate"])
        

class MultiScaleCNN_ParallelBlock(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_ParallelBlock, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)
        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        input=self.permute(input_tensor)
        if self.input_gaussian_stddev > 0:
            input=self.input_gaussian_norm(input, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(input)
            )

        block_01=tf.concat(x_block_01_conv, -1)
        if self.block_01_gaussian_stddev > 0:
            block_01=self.block_01_gaussian_layer(block_01, training=training)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_01(block_01, training=training)
        block_01=self.block_01_deptwise1D(block_01)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_02(block_01, training=training)
        block_01=self.block_01_activation(block_01)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)
        block_02=self.block_02_flat(block_02)

        # Classification
        output=self.block_classification(block_02)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
# Recurrent layer is at the end
class MultiScaleCNN_Parallel_LSTM(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_Parallel_LSTM, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]
        num_units_lstm=int(p.parameters["num_units_lstm"])

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)

        self.block_02_recurrent=tf.keras.layers.LSTM(num_units_lstm, dropout=0.5)

        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        input=self.permute(input_tensor)
        if self.input_gaussian_stddev > 0:
            input=self.input_gaussian_norm(input, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(input)
            )

        block_01=tf.concat(x_block_01_conv, -1)
        if self.block_01_gaussian_stddev > 0:
            block_01=self.block_01_gaussian_layer(block_01, training=training)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_01(block_01, training=training)
        block_01=self.block_01_deptwise1D(block_01)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_02(block_01, training=training)
        block_01=self.block_01_activation(block_01)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)

        block_02_recurrent=self.block_02_recurrent(block_02, training=training)

        #block_02=self.block_02_flat(block_02)

        
        # Classification
        output=self.block_classification(block_02_recurrent)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    

# LSTM extracts features from the ones obtained by MultiScale1D
# then, the output is concatenated with the previous features extracted
class MultiScaleCNN_Parallel_CONVLSTM(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_Parallel_CONVLSTM, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]
        num_units_lstm=int(p.parameters["num_units_lstm"])

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_01_recurrent=tf.keras.layers.LSTM(num_units_lstm, dropout=0.5, return_sequences=True)

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)

        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        input=self.permute(input_tensor)
        if self.input_gaussian_stddev > 0:
            input=self.input_gaussian_norm(input, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(input)
            )

        block_01_conv=tf.concat(x_block_01_conv, -1)
        if self.block_01_gaussian_stddev > 0:
            block_01_conv=self.block_01_gaussian_layer(block_01_conv, training=training)

        block_01_recurrent=self.block_01_recurrent(block_01_conv, training=training)

        block_01_conv_recurent=tf.concat([block_01_conv, block_01_recurrent], -1)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01_conv_recurent=self.block_01_norm_01(block_01_conv_recurent, training=training)
        block_01=self.block_01_deptwise1D(block_01_conv_recurent)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_02(block_01, training=training)
        block_01=self.block_01_activation(block_01)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)

        block_02=self.block_02_flat(block_02)

        # Classification
        output=self.block_classification(block_02)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
# LSTM extracts features from Input
# then, the output is concatenated with Conv1D features extracted
class MultiScaleCNN_Parallel_CONV_LSTM_INPUT(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_Parallel_CONV_LSTM_INPUT, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]
        num_units_lstm=int(p.parameters["num_units_lstm"])

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_01_recurrent=tf.keras.layers.LSTM(num_units_lstm, dropout=0.5, return_sequences=True)

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)

        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        x_permute=self.permute(input_tensor)
        if self.input_gaussian_stddev > 0:
            x_gaussian_01=self.input_gaussian_norm(x_permute, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(x_gaussian_01)
            )

        x_01_conv=tf.concat(x_block_01_conv, -1)

        x_01_recurrent=self.block_01_recurrent(x_gaussian_01, training=training)

        x_conv_recurrent=tf.concat([x_01_conv, x_01_recurrent], -1)
        
        if self.block_01_gaussian_stddev > 0:
            x_conv_recurrent=self.block_01_gaussian_layer(x_conv_recurrent, training=training)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            x_conv_recurrent=self.block_01_norm_01(x_conv_recurrent, training=training)
        x_depth=self.block_01_deptwise1D(x_conv_recurrent)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            x_depth=self.block_01_norm_02(x_depth, training=training)

        block_01=self.block_01_activation(x_depth)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)

        block_02=self.block_02_flat(block_02)

        # Classification
        output=self.block_classification(block_02)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
# LSTM extracts features from Input
# then, the output is concatenated with Conv1D features extracted
class MultiScaleCNN_Parallel_CONV_LSTM_INPUT_NORM(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_Parallel_CONV_LSTM_INPUT_NORM, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]
        num_units_lstm=int(p.parameters["num_units_lstm"])

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        self.block_01_conv1D_layers_activation=tf.keras.layers.Activation('tanh')

        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_01_recurrent=tf.keras.layers.LSTM(num_units_lstm, dropout=0.5, return_sequences=True)

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)

        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        x_permute=self.permute(input_tensor)
        #if self.input_gaussian_stddev > 0:
        #    x_gaussian_01=self.input_gaussian_norm(x_permute, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(x_permute)
            )

        x_01_conv=tf.concat(x_block_01_conv, -1)
        x_01_conv=self.block_01_conv1D_layers_activation(x_01_conv)

        x_01_recurrent=self.block_01_recurrent(x_permute, training=training)

        x_conv_recurrent=tf.concat([x_01_conv, x_01_recurrent], -1)
        
        if self.block_01_gaussian_stddev > 0:
            x_conv_recurrent=self.block_01_gaussian_layer(x_conv_recurrent, training=training)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            x_conv_recurrent=self.block_01_norm_01(x_conv_recurrent, training=training)
        x_depth=self.block_01_deptwise1D(x_conv_recurrent)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            x_depth=self.block_01_norm_02(x_depth, training=training)

        block_01=self.block_01_activation(x_depth)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)

        block_02=self.block_02_flat(block_02)

        # Classification
        output=self.block_classification(block_02)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))

# LSTM extracts features from Input and are mixed at the end
class MultiScaleCNN_Parallel_CONV_LSTM_OUTPUT(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_Parallel_CONV_LSTM_OUTPUT, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]
        num_units_lstm=int(p.parameters["num_units_lstm"])
        num_units_lstm_out=int(p.parameters["num_units_lstm_out"])

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_recurrent_01=tf.keras.layers.LSTM(num_units_lstm, dropout=0.5, return_sequences=True)
        self.block_recurrent_02=tf.keras.layers.LSTM(num_units_lstm_out, dropout=0.5, return_sequences=False)
        self.block_recurent_norm=tf.keras.layers.BatchNormalization()

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)

        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        x_permute=self.permute(input_tensor)
        if self.input_gaussian_stddev > 0:
            x_gaussian_01=self.input_gaussian_norm(x_permute, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(x_gaussian_01)
            )

        x_01_conv=tf.concat(x_block_01_conv, -1)

        #x_01_recurrent=self.block_01_recurrent(x_gaussian_01, training=training)

        #x_conv_recurrent=tf.concat([x_01_conv, x_01_recurrent], -1)
        
        if self.block_01_gaussian_stddev > 0:
            x_01_conv=self.block_01_gaussian_layer(x_01_conv, training=training)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            x_01_conv=self.block_01_norm_01(x_01_conv, training=training)
        x_depth=self.block_01_deptwise1D(x_01_conv)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            x_depth=self.block_01_norm_02(x_depth, training=training)

        block_01=self.block_01_activation(x_depth)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)

        block_02=self.block_02_flat(block_02)

        # PARALLEL BLOCK RECURRENT
        x_block_recurrent=self.block_recurrent_01(x_permute, training=training)
        x_block_recurrent=self.block_recurrent_02(x_block_recurrent, training=training)
        x_block_recurrent=self.block_recurent_norm(x_block_recurrent, training=training)

        x_conv_recurrent=tf.concat([block_02, x_block_recurrent], -1)

        # Classification
        output=self.block_classification(x_conv_recurrent)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))

    
class MHSCLNetBlock(tf.keras.layers.Layer):
  
    def __init__(self,
    filters:int=64, 
    kernel_size:int=64,
    num_convolutional_layers:int=2,
    activation_conv_list:list=[],
    batch_normalization:bool=False,
    activation_dense:str='relu', 
    padding:str='same',
    depth_multiplier:int=1,
    depthwise_constraint=None,
    depthwise_regularizer=None,
    use_bias:bool=True,
    partial_output:int=19,
    pool_size:int=2,
    pool_type:str="AVG",
    dropout_rate:float=0.5,
    block_name:str="MHSCLNetBlock"):

        super(MHSCLNetBlock, self).__init__()

        self.batch_normalization=batch_normalization

        self.conv_layers=[]
        self.norm_layers=[]
        for num_convolutional_layer, activation_conv in zip(range(num_convolutional_layers), activation_conv_list):
            if(batch_normalization):
                self.norm_layers.append(tf.keras.layers.BatchNormalization())
                activation_conv=None

            self.conv_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters=filters, 
                    kernel_size=kernel_size, 
                    padding=padding,
                    depth_multiplier=depth_multiplier,
                    depthwise_constraint=depthwise_constraint,
                    depthwise_regularizer=depthwise_regularizer,
                    activation=activation_conv,
                    use_bias=use_bias,
                    name=f"{block_name}_{num_convolutional_layer:02}"
                )
            )
                
        if(batch_normalization):
            self.conv_activation=tf.keras.layers.Activation('relu')

        if(pool_type=="AVG"):
            self.pool = tf.keras.layers.AveragePooling1D(
                pool_size=pool_size, 
                name=block_name + '_AvgPool')
        elif(pool_type=="MAX"):
            self.pool = tf.keras.layers.MaxPool1D(
                pool_size=pool_size, 
                name=block_name + '_MaxPool')


        self.flat = tf.keras.layers.Flatten(
            name=block_name + '_Flat')

        self.partial_output = tf.keras.layers.Dense(
            partial_output, 
            activation=activation_dense, 
            use_bias=use_bias,
            name=block_name + '_PartialClassifier')

        self.dropout = tf.keras.layers.Dropout(
            dropout_rate, 
            name=block_name + '_Dropout')

    def call(self, input, training=False):
        
        x=input

        if not self.batch_normalization:
            for conv_layer in self.conv_layers:
                x=conv_layer(x)
        else:
            for conv_layer, batch_norm_layer in zip(self.conv_layers, self.norm_layers):
                x=conv_layer(x)
                x=batch_norm_layer(x, training=training)
                
            x=self.conv_activation(x)
            
        x=self.pool(x)
        x=self.flat(x)
        x=self.partial_output(x)
        x=self.dropout(x)

        return x

class MultiScaleCNN(tf.keras.Model):

    def __init__(self, p):
        super(MultiScaleCNN, self).__init__()

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if p.parameters["previous_conv"]=='1':
            self.previous_conv=tf.keras.layers.Conv1D(
                64,
                64,
                padding='same'
            )
            
        self.block_list=[]
        for num_block in range(int(p.parameters["num_pb"])):
            div=2**num_block
            
            # use_bias_test=bool(int(p.parameters["use_bias"]))
            # depth_multiplier_test=int(p.parameters["depth_multiplier"])
            # print(f"Use Bias[{use_bias_test}] - depth_multiplier[{depth_multiplier_test}] - [{div}]")
            
            if p.parameters["conv_depthwise_constraint"]=='None':
                conv_depthwise_constraint=None
            else:
                conv_depthwise_constraint=tf.keras.constraints.max_norm(
                    float(p.parameters["conv_depthwise_constraint"])
                )

            if p.parameters["conv_depthwise_regularizer"]=='None':
                conv_depthwise_regularizer=None
            else:
                conv_depthwise_regularizer=tf.keras.regularizers.L2(
                    l2=float(p.parameters["conv_depthwise_regularizer"])
                )

            # print(conv_depthwise_regularizer)

            self.block_list.append( 
                MHSCLNetBlock(
                    filters=int(p.parameters["filters"]), 
                    kernel_size=int(p.parameters["kernel_size"])//div,
                    num_convolutional_layers=int(p.parameters["num_convolutional_layers"]),
                    activation_conv_list=ast.literal_eval(p.parameters["activation_conv_list"]),
                    batch_normalization=(p.parameters["batch_normalization"]=='1'),
                    activation_dense=p.parameters["activation_dense"], 
                    padding=p.parameters["padding"],
                    depth_multiplier=int(p.parameters["depth_multiplier"]),
                    depthwise_constraint=conv_depthwise_constraint,
                    depthwise_regularizer=conv_depthwise_regularizer,
                    use_bias=bool(int(p.parameters["use_bias"])),
                    partial_output=int(p.parameters["partial_output"]),
                    pool_size=int(p.parameters["pool_size"]),
                    pool_type=p.parameters["pool_type"],
                    dropout_rate=float(p.parameters["dropout_rate"]),
                    block_name=f"PB_{num_block:02}"
                )
            )

        if p.parameters["concat_mode"]=="AVG":
            self.concatBlock = tf.keras.layers.Average(name='Concat_Blocks_Average')
        elif p.parameters["concat_mode"]=="MAX":
            self.concatBlock = tf.keras.layers.Maximum(name='Concat_Blocks_Maximum')
        elif p.parameters["concat_mode"]=="MIN":
            self.concatBlock = tf.keras.layers.Minimum(name='Concat_Blocks_Minimum')
        elif p.parameters["concat_mode"]=="ALL":
            self.concatBlock_avg=tf.keras.layers.Average(name='Concat_Blocks_Average')
            self.concatBlock_max=tf.keras.layers.Maximum(name='Concat_Blocks_Maximum')
            self.concatBlock_min=tf.keras.layers.Minimum(name='Concat_Blocks_Minimum')
            self.concatBlock=tf.keras.layers.Concatenate(name='Concat_Blocks_Concatenate')
        else:
            self.concatBlock=tf.keras.layers.Concatenate(name='Concat_Blocks_Concatenate')

        if p.parameters["attention"]=="1":
            self.attention=tf.keras.layers.Dense(
                int(p.parameters["attention_units"]), 
                activation=p.parameters["attention_activation"],
                use_bias=bool(int(p.parameters["use_bias"])),
                name='Attention')

        self.drop = tf.keras.layers.Dropout(float(p.parameters["dropout_rate"]))

        if p.parameters["dense_kernel_constraint"]=='None':
            dense_kernel_constraint=None
        else:
            dense_kernel_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["dense_kernel_constraint"]))

        if p.parameters["conv_kernel_regularizer"]=='None':
                conv_kernel_regularizer=None
        else:
            conv_kernel_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["conv_kernel_regularizer"])
            )

        # print(conv_kernel_regularizer)
        self.classifier = tf.keras.layers.Dense(
            1, 
            use_bias=bool(int(p.parameters["use_bias"])),
            kernel_constraint=dense_kernel_constraint,
            kernel_regularizer=conv_kernel_regularizer,
            )
        
        self.final_activation=tf.keras.layers.Activation('sigmoid')

    def call(self, input_tensor):

        x = self.permute(input_tensor)

        if self.p.parameters["previous_conv"]=='1':
            x=self.previous_conv(x)

        x_out_pb_list=[]
        for pb in self.block_list:
            x_out_pb_list.append(pb(x))

        if self.p.parameters["concat_mode"]=="ALL":
            x_concat_avg=self.concatBlock_avg(x_out_pb_list)
            x_concat_min=self.concatBlock_min(x_out_pb_list)
            x_concat_max=self.concatBlock_max(x_out_pb_list)
            x_concat=self.concatBlock([x_concat_min, x_concat_avg, x_concat_max])
        else:
            x_concat=self.concatBlock(x_out_pb_list)

        if self.p.parameters["attention"]=="1":
            x_concat=self.attention(x_concat)

        x=self.drop(x_concat)
        x=self.classifier(x)
        x=self.final_activation(x)
        
        return x
    
    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))

class MultiScale_Recurrent(tf.keras.Model):

    def __init__(self, p):
        super(MultiScale_Recurrent, self).__init__()

        self._init_params(p)

        self.permute = tf.keras.layers.Permute((2,1))

        self.conv=tf.keras.layers.Conv1D(
            self.filters_f1,
            self.kernel_size_f1
            ,padding='same')
        
        self.conv_norm=tf.keras.layers.BatchNormalization()
        
        self.dept_conv=tf.keras.layers.DepthwiseConv1D(
            self.depth_kernel_size,
            padding='same',
            depth_multiplier=self.depth_multiplier)

        self.depth_norm=tf.keras.layers.BatchNormalization()

        self.depth_activation=tf.keras.layers.Activation(self.depth_activation)

        self.depth_pool=tf.keras.layers.MaxPool1D(4)

        self.sep_conv=tf.keras.layers.SeparableConv1D(self.filters_conv, self.kernel_size_conv, padding='same')
        self.sep_conv_norm=tf.keras.layers.BatchNormalization()
        self.sep_conv_activation=tf.keras.layers.Activation(self.activation_conv)
        self.sep_conv_pool=tf.keras.layers.MaxPool1D(4)

        self.global_max_conv=tf.keras.layers.GlobalMaxPooling1D()
        self.flat=tf.keras.layers.Flatten()

        self.recurrent_01=tf.keras.layers.LSTM(self.units_lstm_f1, dropout=self.dropout_lstm, return_sequences=True)
        self.recurrent_01_norm=tf.keras.layers.BatchNormalization()
        self.recurrent_01_pool=tf.keras.layers.MaxPool1D()

        self.recurrent_02=tf.keras.layers.LSTM(self.units_lstm_f2, dropout=self.dropout_lstm)
        self.recurrent_02_norm=tf.keras.layers.BatchNormalization()

        self.concat=tf.keras.layers.Concatenate()


        self.block_classification_dense_01=tf.keras.layers.Dense(self.units_classification_dense_01)
        self.block_classification_activation_01=tf.keras.layers.Activation('relu')
        self.block_classification_dense_01_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_02=tf.keras.layers.Dense(self.units_classification_dense_01//2)
        self.block_classification_activation_02=tf.keras.layers.Activation('relu')
        self.block_classification_dense_02_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_03=tf.keras.layers.Dense(self.units_classification_dense_01//4)
        self.block_classification_activation_03=tf.keras.layers.Activation('relu')
        self.block_classification_dense_03_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_04=tf.keras.layers.Dense(self.units_classification_dense_01//8)
        self.block_classification_activation_04=tf.keras.layers.Activation('relu')
        self.block_classification_dense_04_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_out_norm=tf.keras.layers.BatchNormalization()
        self.block_classification_dense_out=tf.keras.layers.Dense(1)
        self.block_classification_activation_out=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        p=self.permute(input_tensor)

        x=self.conv(p)
        x=self.conv_norm(x, training=training)

        x=self.dept_conv(x)
        x=self.depth_norm(x, training=training)
        x=self.depth_activation(x)
        x=self.depth_pool(x)

        x=self.sep_conv(x)
        x=self.sep_conv_norm(x, training=training)
        x=self.sep_conv_activation(x)
        x=self.sep_conv_pool(x)

        x=self.global_max_conv(x)
        #x=self.flat(x)

        r=self.recurrent_01(p, training=training)
        r=self.recurrent_01_norm(r, training=training)
        r=self.recurrent_01_pool(r)

        r=self.recurrent_02(r, training=training)
        r=self.recurrent_02_norm(r, training=training)

        c=self.concat([x,r])

        c=self.block_classification_dense_01(c)
        c=self.block_classification_activation_01(c)
        c=self.block_classification_dense_01_drop(c)

        c=self.block_classification_dense_02(c)
        c=self.block_classification_activation_02(c)
        c=self.block_classification_dense_02_drop(c)

        c=self.block_classification_dense_03(c)
        c=self.block_classification_activation_03(c)
        c=self.block_classification_dense_03_drop(c)

        c=self.block_classification_dense_04(c)
        c=self.block_classification_activation_04(c)
        c=self.block_classification_dense_04_drop(c)

        c=self.block_classification_dense_out_norm(c, training=training)
        c=self.block_classification_dense_out(c)
        c=self.block_classification_activation_out(c)

        return c

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
    def _init_params(self, p):

        self.p=p
        
        self.filters_f1=int(p.parameters["filters_f1"])
        self.kernel_size_f1=int(p.parameters["kernel_size_f1"])

        self.units_lstm_f1=int(p.parameters["units_lstm_f1"])
        self.dropout_lstm=float(p.parameters["dropout_lstm"])
        self.units_lstm_f2=int(p.parameters["units_lstm_f2"])

        self.depth_kernel_size=int(p.parameters["depth_kernel_size"])
        self.depth_multiplier=int(p.parameters["depth_multiplier"])
        self.depth_activation=p.parameters["depth_activation"]

        self.filters_conv=int(p.parameters["filters_conv"])
        self.kernel_size_conv=int(p.parameters["kernel_size_conv"])
        self.activation_conv=p.parameters["activation_conv"]

        self.units_classification_dense_01=int(p.parameters["units_classification_dense_01"])


class DeepEEGNet(tf.keras.Model):

    def __init__(self, p):
        super(DeepEEGNet, self).__init__()

        self._init_params(p)
        self.permute = tf.keras.layers.Permute((2,1))

        self.block_01_conv1D=tf.keras.layers.Conv1D(
            128, 
            64,
            padding="same")
        
        self.pool_01 = tf.keras.layers.MaxPool1D()
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation('gelu')
        self.block_01_drop=tf.keras.layers.Dropout(0.4)

        self.block_02_conv1D=tf.keras.layers.Conv1D(
            64, 
            32)
        
        self.pool_02 = tf.keras.layers.MaxPool1D()
        self.block_02_drop=tf.keras.layers.Dropout(0.4)
        
        self.block_03_conv1D=tf.keras.layers.Conv1D(
            32, 
            16)
        self.pool_03 = tf.keras.layers.MaxPool1D()

        self.block_04_conv1D=tf.keras.layers.Conv1D(
            16, 
            8)
        self.pool_04 = tf.keras.layers.MaxPool1D()

        self.block_05_conv1D=tf.keras.layers.Conv1D(
            8, 
            4)
        self.pool_05 = tf.keras.layers.MaxPool1D()

        self.block_06_conv1D=tf.keras.layers.DepthwiseConv1D(
            4, 
            2)
        self.pool_06 = tf.keras.layers.MaxPool1D()
        self.block_06_norm_01=tf.keras.layers.BatchNormalization()
        self.block_06_activation=tf.keras.layers.Activation('gelu')
        self.block_06_drop=tf.keras.layers.Dropout(0.4)

        self.block_flat=tf.keras.layers.Flatten(name = 'flatten')

        self.block_classification_dense=tf.keras.layers.Dense(
            1)
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        x=self.permute(input_tensor)
        
        x=self.block_01_conv1D(x)
        x=self.pool_01(x)
        x=self.block_01_norm_01(x, training=training)
        x=self.block_01_activation=(x)
        x=self.block_01_drop=(x)

        #x=self.block_02_conv1D(x)
        #x=self.pool_02(x)
        #x=self.block_02_drop(x)
        #x=self.block_03_conv1D(x)
        #x=self.pool_03(x)

        #x=self.block_04_conv1D(x)
        #x=self.pool_04(x)

        #x=self.block_05_conv1D(x)
        #x=self.pool_05(x)

        x=self.block_06_conv1D(x)
        x=self.pool_06(x)
        x=self.block_06_norm_01(x, training=training)
        x=self.block_06_activation=(x)
        x=self.block_06_drop=(x)
        
        x=self.block_flat(x)
        x=self.block_classification_dense(x)
        x=self.block_classification_activation(x)

        return x

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
    def _init_params(self, p):
        
        pass

class EEGNet(tf.keras.Model):

    def __init__(self, p):
        super(EEGNet, self).__init__()

        self.permute = tf.keras.layers.Permute((2,1))

        # Block 01
        self.block_01_conv2D=tf.keras.layers.Conv2D(
            int(p.parameters["F1"]), 
            (1, int(p.parameters["kernLength"])), 
            padding = 'same',
            use_bias = False)
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()
        self.block_01_deptwise2D=tf.keras.layers.DepthwiseConv2D(
            (int(p.parameters["Chans"]), 1), 
            use_bias = False, 
            depth_multiplier = int(p.parameters["D"]),
            depthwise_constraint = tf.keras.constraints.max_norm(1.))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation_01=tf.keras.layers.Activation('elu')
        self.block_01_pool=tf.keras.layers.AveragePooling2D((1, 4))
        self.block_01_drop=tf.keras.layers.Dropout(float(p.parameters["dropoutRate"]))

        # Block 02

        self.block_02_sepConv2D=tf.keras.layers.SeparableConv2D(
            int(p.parameters["F2"]), 
            (1, 16),
            use_bias = False, 
            padding = 'same')
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation('elu')
        self.block_02_pool=tf.keras.layers.AveragePooling2D((1, 8))
        self.block_02_drop=tf.keras.layers.Dropout(float(p.parameters["dropoutRate"]))
        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')


        # Classification

        self.block_classification_dense=tf.keras.layers.Dense(
            1, 
            kernel_constraint = tf.keras.constraints.max_norm(float(p.parameters["norm_rate"])))
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')
        
    def call(self, input_tensor, training=False):

        permute_tensor=self.permute(input_tensor)
        expanded_tensor=tf.expand_dims(permute_tensor, -1, name='Adapting_2D')

        block1=self.block_01_conv2D(expanded_tensor)
        block1=self.block_01_norm_01(block1, training=training)
        block1=self.block_01_deptwise2D(block1)
        block1=self.block_01_norm_02(block1, training=training)
        block1=self.block_01_activation_01(block1)
        block1=self.block_01_pool(block1)
        block1=self.block_01_drop(block1)

        block2=self.block_02_sepConv2D(block1)
        block2=self.block_02_norm_01(block2, training=training)
        block2=self.block_02_activation_01(block2)
        block2=self.block_02_pool(block2)
        block2=self.block_02_drop(block2)
        
        flatten=self.block_02_flat(block2)
        
        dense=self.block_classification_dense(flatten)
        output=self.block_classification_activation(dense)

        return output
        
    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))

"""
def EEGNet(nb_classes, Chans = 64, Samples = 128, 
             dropoutRate = 0.5, kernLength = 64, F1 = 8, 
             D = 2, F2 = 16, norm_rate = 0.25, dropoutType = 'Dropout'):
    
    if dropoutType == 'SpatialDropout2D':
        dropoutType = SpatialDropout2D
    elif dropoutType == 'Dropout':
        dropoutType = Dropout
    else:
        raise ValueError('dropoutType must be one of SpatialDropout2D '
                         'or Dropout, passed as a string.')
    
    input1   = Input(shape = (Chans, Samples, 1))

    ##################################################################
    block1       = Conv2D(F1, (1, kernLength), padding = 'same',
                                   input_shape = (Chans, Samples, 1),
                                   use_bias = False)(input1)
    block1       = BatchNormalization()(block1)
    block1       = DepthwiseConv2D((Chans, 1), use_bias = False, 
                                   depth_multiplier = D,
                                   depthwise_constraint = max_norm(1.))(block1)
    block1       = BatchNormalization()(block1)
    block1       = Activation('elu')(block1)
    block1       = AveragePooling2D((1, 4))(block1)
    block1       = dropoutType(dropoutRate)(block1)
    
    block2       = SeparableConv2D(F2, (1, 16),
                                   use_bias = False, padding = 'same')(block1)
    block2       = BatchNormalization()(block2)
    block2       = Activation('elu')(block2)
    block2       = AveragePooling2D((1, 8))(block2)
    block2       = dropoutType(dropoutRate)(block2)
        
    flatten      = Flatten(name = 'flatten')(block2)
    
    if nb_classes == 2:
        dense     = Dense(1, kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('sigmoid', name = 'sigmoid')(dense)

    else:
        dense        = Dense(nb_classes, name = 'dense', 
                            kernel_constraint = max_norm(norm_rate))(flatten)
        modeloutput  = Activation('softmax', name = 'softmax')(dense)
    
    return Model(inputs=input1, outputs=modeloutput)

"""
