import os
import sys
import csv


class CsvModelParams():
    def __init__(self, model_name, id_parameters_value=-1):
        
        self.model_name = model_name
        self.id_parameters_value=id_parameters_value

        self.csv_parameters_file=os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","conf", F"{self.model_name}.csv")

        if not os.path.exists(self.csv_parameters_file):
            print(f"File not found [{self.csv_parameters_file}]. Exiting.")
            sys.exit()

        self.parameters=self._load_parameters_csv()

        print(self.parameters)

    def _load_parameters_csv(self):

        if os.path.exists(self.csv_parameters_file):
            with open(self.csv_parameters_file, mode='r', newline='') as csvfile:
                reader = csv.DictReader(csvfile)
                found=False

                for row in reader:
                    if int(row['id'])==self.id_parameters_value:
                        return row.copy()

                if not found:
                    print(f"Id parameter not found [{self.id_parameters_value}]")

        else:
            print(f"[{self.csv_parameters_file}] does not exists. Continue with default values")


    def get_params_name(self):

        names = ["model_name", "time"] + [key for key in self.parameters.keys()][1:] + ["ix_iteration", "ix_fold"]
        # print("NAMES")
        # print(names)
        # index = 1
        # for n in names:
        #     print(f"[{index:03d}] - {n}")
        #     index += 1

        return names

    def get_params_values(self, time, num_iter, num_fold):

        values = [self.model_name, time] + [val for key, val in self.parameters.items()][1:] + [num_iter, num_fold]

        # index = 1
        # for v in values:
        #     print(f"[{index:03d}] - {v}")
        #     index += 1

        return values

if __name__ == "__main__":

    testParam = CsvModelParams("MultiScaleCNN_parallelBlock", 252)
    names = testParam.get_params_name()
    values = testParam.get_params_values(33, 44, 55)

    index=1
    for n, v in zip(names, values):
        print(f"[{index:03d}] - {n} {v}")
        index += 1