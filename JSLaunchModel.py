import os
import sys
import gc
import time
import imp
import argparse
import ast
import json

import numpy as np
import tensorflow as tf


from sklearn.model_selection import KFold
from sklearn.utils import shuffle
from tensorflow import keras

# from JSConvolutionalLayers import SPCnnModel, SPCnnModelBn

dir_path = os.path.dirname(os.path.realpath(__file__))

sys.path.append(os.path.join(dir_path, "lib", "JSData"))
sys.path.append(os.path.join(dir_path, "lib", "JSPreprocessing"))
sys.path.append(os.path.join(dir_path, "lib", "JSLog"))


from JSDataAdhd import JSDataAdhd
from JSPreprocessingAdhd import JSPreprocessingAdhd
from JSLog import JSLog

from CsvModelParams import CsvModelParams

class JSLaunchModel:

    def __init__(self, model_name=None, configuration_file_name=None, configuration_id=0, int_seed=1234):

        

        self.configuration_id = configuration_id
        self.model_name=model_name
        self.conf_file=configuration_file_name if configuration_file_name else self.model_name
        
        self.p = CsvModelParams(self.conf_file, configuration_id)

        self.seed=int(self.p.parameters["int_seed"])
        self.model_class = self._load_class(model_name)
        self.experiment_name = f"{self.model_name}_{self.configuration_id:02}"

        self.jsLog = JSLog(f"{self.model_name}_{self.experiment_name}")

        self.jsData = JSDataAdhd(self.experiment_name, int_seed=self.seed)

        self.load_model_data(self.p.parameters["model_data_type"])

        gc.collect()

    def load_model_data(self, model_data_type):

        truncate=-1
        if 'truncate' in self.p.parameters:
            truncate=int(self.p.parameters["truncate"])

        window=None
        if 'window' in self.p.parameters:
            if self.p.parameters["window"] != 'None':
                window=self.p.parameters["window"]

        if model_data_type=="raw":
            self.jsData.load_model_data_raw(
                self.p.parameters["preprocessing_type"], 
                window_size=float(self.p.parameters["window_size"]), 
                overlapping=float(self.p.parameters["overlapping"]), 
                standarize=bool(int(self.p.parameters["standarize"])),
                truncate=truncate,
                window=window,
                outliers=self.p.parameters["outliers"])

        else:

            self.jsData.load_s_transform_data(
                self.p.parameters["preprocessing_type"], 
                int(self.p.parameters["window_width"]), 
                int(self.p.parameters["window_overlapping"]),
                fmin=int(self.p.parameters["min_freq"]), 
                fmax=int(self.p.parameters["max_freq"]), 
                img_shape=(int(self.p.parameters["img_height"]),int(self.p.parameters["img_width"])), 
                save_data = True,
                truncate=truncate)

        

    def test_model(self, input_shape, input_shape_build):

        model=self.model_class(self.p)
        model.build(input_shape=input_shape)
        model.model(shape=input_shape_build).summary()

    def launch_model(self):

        self._write_init_logs()

        num_epochs = int(self.p.parameters["num_epochs"])
        batch_size = int(self.p.parameters["batch_size"])

        print('##################################')
        print('##################################')
        print('##################################')
        print(self.p.parameters["callback_list"])
        print('##################################')
        print('##################################')
        print('##################################')

        if self.p.parameters["callback_list"]=='None':
            callbacks=None
            print("Callbacks None")
        else:
            callbacks=[]
            for callback in ast.literal_eval(self.p.parameters["callback_list"]):
                print(callback)
                if callback=='EarlyStopping':
                    callbacks.append(tf.keras.callbacks.EarlyStopping(monitor='loss', patience=3, mode='min', verbose=2))
        
        metrics=[
            keras.metrics.BinaryAccuracy(name="binary_accuracy"),
            keras.metrics.Precision(name="precision"),
            keras.metrics.Recall(name="recall"),
            keras.metrics.TruePositives(name="true_positives"),
            keras.metrics.TrueNegatives(name="true_negatives"),
            keras.metrics.FalsePositives(name="false_positives"),
            keras.metrics.FalseNegatives(name="false_negatives")
            ]

        for num_iter in np.arange(int(self.p.parameters["num_iter"])):
            
            self.jsLog.w(f"-----------------------------------------------------------------------")
            self.jsLog.w(f"######################## START ITER [{num_iter}] ########################")
            

            self._perform_iteration(num_iter, num_epochs, batch_size, callbacks, metrics)



            self.jsLog.w(f"######################## END ITER [{num_iter}] ########################")
            self.jsLog.w(f"-----------------------------------------------------------------------")
        
        self.jsLog.compute_results()

    def _perform_iteration(self, num_iter, num_epochs, batch_size, callbacks, metrics):

        num_fold = 0
        num_subjects = 61
        kf = KFold(n_splits=int(self.p.parameters["n_splits"]), shuffle=True, random_state=self.seed)
        for train_index, test_index in kf.split(np.arange(num_subjects)):
            self.jsLog.w(f"---------------------------------------------------")
            self.jsLog.w(f"############# START FOLD [{num_fold}] #############")

            start_time = time.time()
        
            train_index += 1
            test_index += 1

            X_train, y_train, X_val, y_val, X_test, y_test, train_index, val_index, test_index = self.jsData.get_train_test_val_model_data(train_index, test_index, normalize=bool(int(self.p.parameters["normalize"])), num_validation_subjects=int(self.p.parameters['num_validation_subjects']))
            gc.collect()

            self._write_subjects_logs(num_fold, train_index=train_index, val_index=val_index, test_index=test_index)
            self._write_data_logs(X_train, y_train, X_val, y_val, X_test, y_test)

            jsModel=self.model_class(self.p)
        
            jsModel.compile(
                optimizer=self._get_optimizer(),
                loss=keras.losses.BinaryCrossentropy(),
                metrics=metrics
            )

            start_fit = time.time()
            history = jsModel.fit(
                X_train, 
                y_train, 
                batch_size=batch_size, 
                epochs=num_epochs, 
                validation_data=(X_val, y_val),
                callbacks=callbacks,
                verbose=2
            )

            results = jsModel.evaluate(X_test, y_test, batch_size=batch_size, verbose=2)
            self._write_results_log(num_fold, results)
            end_fit = time.time()

            self.jsLog.save_history(
                self.p.get_params_name(), 
                self.p.get_params_values(end_fit - start_fit,num_iter, num_fold), 
                history)

            self.jsLog.save_results(
                self.p.get_params_name(),
                self.p.get_params_values(end_fit - start_fit,num_iter, num_fold),
                results)
            
            for index_subject in test_index:
                for type_subject in [0,1]:
                    
                    X_test, y_test = self.jsData.get_train_model_data_by_subject_type(index_subject, type_subject)

                    self.jsLog.w(f"SHAPES X_test[{X_test.shape[0]}][{X_test.shape[1]}]", show=True)
                    self.jsLog.w(f"SHAPES y_test[{y_test.shape[0]}]", show=True)
                    if (X_test.shape[0] > 0):
                        self.jsLog.w(f"EVALUATING BY SUBJECT [{index_subject}][{type_subject}]", show=True)
                        self.jsLog.w(f"\tLABELS SUBJECT LEN[{y_test.shape}] MAX[{y_test.max()}] MIN[{y_test.min()}] MEAN[{y_test.mean()}]")

                        predictions_subject = jsModel.predict(X_test, batch_size=batch_size)
                        self.jsLog.save_results_by_subject_type(
                            self.p.get_params_name(),
                            self.p.get_params_values(0,num_iter, num_fold),
                            predictions_subject,
                            index_subject,
                            type_subject
                            )
                    else:
                        self.jsLog.w(f"SUBJECT DISCARDED BY LOW DATA[{index_subject}][{type_subject}]", show=True)

            del(X_train)
            del(X_val)
            del(X_test)
            del(y_train)
            del(y_val)
            del(y_test)
            del(jsModel)
            gc.collect()

            end_time = time.time()
            self.jsLog.w(f"TOTAL TIME FOLD[{num_fold}] [{(end_time-start_time)/60}]min - EPOCHS [{num_epochs}]")
            self.jsLog.w(f"---------------------------------------------------")
            self.jsLog.w(f"############# END FOLD [{num_fold}] ###############")
            num_fold += 1

    def _get_optimizer(self):

        optimizer=keras.optimizers.Adam()
        try:
            optimizer_str=self.p.parameters["optimizer"]

            if optimizer_str != None and optimizer_str != "None":
                epsilon=1e-7
                learning_rate=0.001
                try:
                    optimizer_metadata=json.loads(optimizer_str.replace("'", "\""))
                    epsilon=optimizer_metadata["epsilon"]
                    learning_rate=optimizer_metadata["learning_rate"]
                    beta_1=optimizer_metadata['beta_1'] if "beta_1" in optimizer_metadata else 0.9
                    beta_2=optimizer_metadata['beta_2'] if "beta_2" in optimizer_metadata else 0.999

                    self.jsLog.w(f"[OPTIMIZER META DATA] - [{epsilon}] - [{learning_rate}] - [{beta_1}] - [{beta_2}]", True)
                    if (optimizer_metadata["name"]=="Adam"):
                        optimizer=keras.optimizers.Adam(learning_rate=learning_rate, beta_1=beta_1, beta_2=beta_2, epsilon=epsilon)
                    if (optimizer_metadata["name"]=="AdamW"):
                        optimizer=keras.optimizers.AdamW(learning_rate=learning_rate, epsilon=epsilon)
                except Exception as err:
                    self.jsLog.w(f"[ERROR] - [{err}]", True)
        except Exception as err:
            self.jsLog.w(f"[ERROR] - Optimizer not in CSV [{err}] - USING Adam with default values", True)
            optimizer=keras.optimizers.Adam()

        return optimizer

    def _write_results_log(self, num_fold, results):
        self.jsLog.w(f"RESULTS FOLD [{num_fold}] [{results}]")
        

    def _write_data_logs(self, X_train, y_train, X_val, y_val, X_test, y_test):
        
        self.jsLog.w(f"X_train statistics: [{X_train.shape}][{y_train.shape}] Max[{X_train.max()}] Min[{X_train.min()}] | Std[{X_train.std()}]")
        self.jsLog.w(f"X_val statistics: [{X_val.shape}][{y_val.shape}] Max[{X_val.max()}] Min[{X_val.min()}] | Std[{X_val.std()}]")
        self.jsLog.w(f"X_test statistics: [{X_test.shape}][{y_test.shape}] Max[{X_test.max()}] Min[{X_test.min()}] | Std[{X_test.std()}]")
        self.jsLog.w(f"Computed Model Data shape [{self.jsData.model_data.shape}] | [{self.jsData.model_data.dtype}] | max [{self.jsData.model_data.max()}] | min [{self.jsData.model_data.min()}] | mean [{self.jsData.model_data.mean()} | std [{self.jsData.model_data.std()}]")
    
    def _write_subjects_logs(self, num_fold:int, train_index:list=[], val_index:list=[], test_index:list=[]):
        self.jsLog.w(f" -- SUBJECTS FOLD [{num_fold}]")
        if len(train_index)>0:
            string_index_train=""
            for item in train_index:
                string_index_train+=f"{item} "
            self.jsLog.w(f"\tTRAIN [{len(train_index)}] [{string_index_train}]")
        
        if len(val_index)>0:
            self.jsLog.w(f"\tVAL   [{len(val_index)}] {val_index}")

        if len(test_index)>0:
            self.jsLog.w(f"\tTEST  [{len(test_index)}] {test_index}")

    def _write_init_logs(self):

        self.jsLog.w(f"JSDATA: type [{type(self.jsData.data)}] - len [{len(self.jsData.data)}] - Model SPCnnModelBn with BatchNorm and constraints")

        # print(f"ASR [{len(jsData.data['preprocessed_asr'])}] - CONTROLS [{len(jsData.data['preprocessed_asr']['CONTROLS'])}] - ADHD [{len(jsData.data['preprocessed_asr']['ADHD'])}]")

        self.jsLog.w(f"Computed Model Data shape [{self.jsData.model_data.shape}] | [{self.jsData.model_data.dtype}] | max [{self.jsData.model_data.max()}] | min [{self.jsData.model_data.min()}] | mean [{self.jsData.model_data.mean()} | std [{self.jsData.model_data.std()}]")

        self.jsLog.w(f"Computed Labels shape [{self.jsData.model_labels.shape}] | [{self.jsData.model_labels.dtype}]")

        self.jsLog.w(f"---------------------------------------------")
        self.jsLog.w(f"Prueba SPCnnModelBn {self.configuration_id:02} {self.p.parameters['num_epochs']} epochs and {self.p.parameters['batch_size']} batch size")

        self.jsLog.w(f"############ MODEL DESCRIPTION ##############")
        self.jsLog.w(f"---------------------------------------------")


    def _load_class(self, full_class_string):
        """
        Dynamically load a class from a string
            klass = load_class("module.submodule.ClassName")
            klass2 = load_class("myfile.Class2")
        """
        class_data = full_class_string.split(".")
        
        module_str = class_data[0]
        class_str = class_data[-1]
        submodules_list = []

        if len(class_data) > 2:
            submodules_list = class_data[1:-1]

        f, filename, description = imp.find_module(module_str)
        module = imp.load_module(module_str, f, filename, description)
        
        # Find each submodule
        for smod in submodules_list:
            path = os.path.dirname(filename) if os.path.isfile(filename) else filename

            f, filename, description = imp.find_module(smod, [path])
            
            # Now we can load the module
            try:
                module = imp.load_module(" ".join(class_data[:-1]), f, filename, description)
            finally:
                if f:
                    f.close()

        # Finally, we retrieve the Class
        return getattr(module, class_str)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        prog = 'ProgramName',
        description = 'What the program does',
        epilog = 'Text at the bottom of help')

    parser.add_argument('-i', '--id')
    parser.add_argument('-m', '--modelclass')
    parser.add_argument('-p', '--modelpackage')
    args = parser.parse_args()

    print(args.id)
    print(args.modelclass)
    input_shape=(None, 19, 256)
    input_shape_build=(19, 256)
    #launch=JSLaunchModel('JSADHDModels.MultiScaleCNN', configuration_id=id)
    ids=args.id.split(",")
    for id in range(int(ids[0]), int(ids[1])):
        print(id)
    
        launch=JSLaunchModel(
            model_name=f"{args.modelpackage}.{args.modelclass}", 
            configuration_file_name=args.modelclass, 
            configuration_id=id)

        launch.test_model(input_shape, input_shape_build)
        launch.launch_model()

        launch=None
        gc.collect()
            