import sys
import os
import mne
import time
import argparse

import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from scipy.io import loadmat
from JSLib.JSDataAdhd import JSDataAdhd
from JSLib.JSLog import JSLog
from JSLib.JSADHDModels import MultiScaleCNN, EEGNet
from JSLib.JSMODMAModels import MultiScaleCNN_LSTM
from JSLib.CsvModelParams import CsvModelParams

from mne_faster import (find_bad_channels, find_bad_epochs,
                        find_bad_components, find_bad_channels_in_epochs)


DATA_DIR="/workspace/data/data/modma"
LOG_DIR="/workspace/data/logs/modma"

# DATA_DIR="/Volumes/extJavier/DOCTORAT/data/EEG_128channels_ERP_lanzhou_2015"
# LOG_DIR="/Volumes/extJavier/DOCTORAT/logs/modma"

def plot_mask(mask_test, mask_train, mask_val):
    
    plt.figure(1)
    plt.subplot(311)
    plt.plot(mask_test)
    plt.subplot(312)
    plt.plot(mask_train)
    plt.subplot(313)
    plt.plot(mask_val)
    plt.show()

def load_montage():
    montage_mat_file_name=os.path.join(DATA_DIR, "chan_info_egi_128.mat")
    montage_mat_data=loadmat(montage_mat_file_name)

    print(montage_mat_data["__header__"])
    print(montage_mat_data["__version__"])
    print(montage_mat_data["__globals__"])

    mat_chanlocs=montage_mat_data["chanlocs"]

    print(f"Type [{type(mat_chanlocs)}] - Shape [{mat_chanlocs.shape}]")

    epsilon=1.0e-14

    to_dict_montage={}
    for row in mat_chanlocs[0]:
        y=np.squeeze(row[1])*-1 if np.abs(np.squeeze(row[1])) > epsilon else 0
        x=np.squeeze(row[2]) if np.abs(np.squeeze(row[2])) > epsilon else 0
        z=np.squeeze(row[3]) if np.abs(np.squeeze(row[3])) > epsilon else 0
        
        to_dict_montage[np.squeeze(row[0][0])]=np.asarray([y, x, z])/100

    return mne.channels.make_dig_montage(to_dict_montage)

def load_data(jsLog, preprocessing_type="faster"):

    data_file_path=os.path.join(DATA_DIR, "modma_full__.npy")
    data_file_path_filtered=os.path.join(DATA_DIR, "modma_full_filtered__.npy")
    statistics_file_path=os.path.join(DATA_DIR, "modma_stats.csv")

    if os.path.exists(data_file_path) and os.path.exists(data_file_path_filtered):
        
        if preprocessing_type=="faster":
            with open(data_file_path, 'rb') as f:
                model_data = np.load(f)
                sample_labels = np.load(f)
                subject_labels = np.load(f)
        
        elif preprocessing_type=="filtered":
            with open(data_file_path_filtered, 'rb') as f:
                model_data = np.load(f)
                sample_labels = np.load(f)
                subject_labels = np.load(f)

    else:

        data_info=pd.read_excel(os.path.join(DATA_DIR, 'subjects_information_EEG_128channels_ERP_lanzhou_2015.xlsx'))[["subject id", "type"]].copy()

        montage=load_montage()

        ids_subjects=[]
        ix_subject=1
        sample_labels=[]
        subject_labels=[]

        sample_labels_filtered=[]
        subject_labels_filtered=[]


        data=[]
        statistics=[]
        for path in os.scandir(DATA_DIR):
            if path.is_file() and path.name.endswith(".raw"):
                
                # Read the data
                
                id_subject=int(path.name[0:8])
                type_subject=data_info.loc[data_info['subject id']==id_subject]["type"].values[0]
                id_type_subject=0 if type_subject == "HC" else 1
                jsLog.w(f"[{ix_subject}][{id_subject}][{type_subject}][{id_type_subject}] - {path.name}",True)
                ids_subjects.append(path.name[0:8])

                original_eeg_data=mne.io.read_raw_egi(os.path.join(DATA_DIR, path.name), preload=True)
                
                eeg_samples, eeg_times = original_eeg_data[:,:]
                jsLog.w(f"[{type(eeg_samples)}][{eeg_samples.shape}] [{eeg_samples.min()}] [{eeg_samples.max()}] [{eeg_samples.mean()}] [{eeg_samples.std()}]", True)
                
                eeg_channels_data=original_eeg_data.copy().pick_types(meg=False, eeg=True, eog=False).drop_channels("E129")
                eeg_channels_data.set_montage(montage)

                ## Filter the data

                eeg_channels_data_filtered=eeg_channels_data.copy()

                eeg_channels_data_filtered.set_eeg_reference(ref_channels="average", projection=True)
                eeg_channels_data_filtered.filter(l_freq=1.0, h_freq=None)
                eeg_channels_data_filtered.filter(l_freq=None, h_freq=100)
                
                eeg_channels_data_filtered.notch_filter(np.arange(50, 125, 50), trans_bandwidth=1.0)

                ## Clean the data using FASTER

                eeg_channels_faster=eeg_channels_data_filtered.copy()
                eeg_channels_faster.info['bads']=[] 
                eeg_channels_faster, _ = mne.io.set_eeg_reference(eeg_channels_faster, [])

                cue_events = mne.find_events(original_eeg_data, stim_channel=["scue", "fcue", "hcue"])

                eeg_epochs = mne.Epochs(eeg_channels_faster, cue_events, tmin=-0.1, tmax=0.5, baseline=(None, 0), preload=True)
                
                # Save filtered epochs before before faster
                jsLog.w(f"Saving Filtered data", True)

                np_eeg_epochs=eeg_epochs.get_data().copy()
                if ix_subject==1:
                    model_data_filtered = np_eeg_epochs
                else:
                    model_data_filtered = np.vstack((model_data_filtered, np_eeg_epochs))

                num_epochs_before=np_eeg_epochs.shape[0]

                sample_labels_filtered+=[[ix_subject, id_subject, type_subject, id_type_subject, path.name]]*num_epochs_before
                subject_labels_filtered+=[[ix_subject,id_type_subject]]

                jsLog.w(f"SHAPE Filtered Data: [{model_data_filtered.shape}] LABELS: [{len(sample_labels_filtered)}]", True)

                ### Step 1: mark bad channels
                eeg_epochs.info['bads'] = find_bad_channels(eeg_epochs, eeg_ref_corr=False)
                if len(eeg_epochs.info['bads']) > 0:
                    eeg_epochs.interpolate_bads()

                ### Step 2: mark bad epochs
                bad_epochs = find_bad_epochs(eeg_epochs)
                if len(bad_epochs) > 0:
                    eeg_epochs.drop(bad_epochs)

                ### Step 3: mark bad ICA components (using the build-in MNE functionality for this)
                ica = mne.preprocessing.ICA(0.99).fit(eeg_epochs)
                ica.exclude = find_bad_components(ica, eeg_epochs, use_metrics=['kurtosis', 'power_gradient', 'hurst', 'median_gradient'])
                ica.apply(eeg_epochs)
                # Need to re-baseline data after ICA transformation
                eeg_epochs.apply_baseline(eeg_epochs.baseline)
                                    
                ### Step 4: mark bad channels for each epoch and interpolate them.
                bad_channels_per_epoch = find_bad_channels_in_epochs(eeg_epochs, eeg_ref_corr=False)
                for i, b in enumerate(bad_channels_per_epoch):
                    if len(b) > 0:
                        ep = eeg_epochs[i]
                        ep.info['bads'] = b
                        ep.interpolate_bads() 
                        eeg_epochs._data[i, :, :] = ep._data[0, :, :]

                np_epochs_data=eeg_epochs.get_data()
                print(np_epochs_data.shape)

                if ix_subject==1:
                    model_data = np_epochs_data
                else:
                    model_data = np.vstack((model_data, np_epochs_data))

                np_data_filtered=eeg_channels_data_filtered.get_data()
                print(f"[{ix_subject}][{id_subject}][{type_subject}] - {path.name}")
                sample_labels+=[[ix_subject, id_subject, type_subject, id_type_subject, path.name]]*np_epochs_data.shape[0]
                subject_labels+=[[ix_subject,id_type_subject]]
                statistics+=[[id_subject, id_type_subject, 
                            eeg_samples.max(), eeg_samples.min(), eeg_samples.mean(), eeg_samples.std(),
                            np_data_filtered.shape[0],np_data_filtered.max(), np_data_filtered.min(), np_data_filtered.mean(), np_data_filtered.std(),
                            num_epochs_before, np_epochs_data.shape[0], np_epochs_data.max(), np_epochs_data.min(), np_epochs_data.mean(), np_epochs_data.std()]]
                
                print(len(sample_labels))
                print(len(model_data))
                print(len(subject_labels))

                ix_subject+=1

        # Convert to numpy
        sample_labels=np.asarray(sample_labels)
        subject_labels=np.asarray(subject_labels)

        with open(data_file_path, 'wb') as f:
            np.save(f, model_data)
            np.save(f, sample_labels)
            np.save(f, subject_labels)
        
        with open(data_file_path_filtered, 'wb') as f:
            np.save(f, model_data_filtered)
            np.save(f, sample_labels_filtered)
            np.save(f, subject_labels_filtered)

        pd_statistics=pd.DataFrame(statistics, columns=["IX", "TYPE", 
                                                        "O_MAX", "O_MIN", "O_MEAN", "O_STD",
                                                        "NUM_F", "F_MAX", "F_MIN", "F_MEAN", "F_STD", 
                                                        "NUM_EB", "NUM_EA", "E_MAX", "E_MIN", "E_MEAN", "E_STD"])
        pd_statistics.to_csv(statistics_file_path)

    if preprocessing_type=="faster":
        with open(data_file_path, 'rb') as f:
            model_data = np.load(f)
            sample_labels = np.load(f)
            subject_labels = np.load(f)
    
    elif preprocessing_type=="filtered":
        with open(data_file_path_filtered, 'rb') as f:
            model_data = np.load(f)
            sample_labels = np.load(f)
            subject_labels = np.load(f)                                             

    return model_data, sample_labels, subject_labels

    
def get_train_val_test(jsLog, model_data, sample_labels, subject_labels, ix_test, normalization=0):

    unique_ix=subject_labels[:,0]

    mask_test=np.isin(sample_labels[:,0], ix_test)
    print(mask_test.sum())
    x_test=model_data[mask_test]
    y_test=sample_labels[mask_test,1]

    print(x_test.shape)
    print(f"{y_test.shape} {sum(y_test)}")

    # The test subject is delete
    ix_train=np.delete(unique_ix, np.where(unique_ix == ix_test))
    # ix_train=np.delete(unique_ix, int(ix_test)-1, 0)
    print(ix_train)

    ### First generate the two groups
    ix_train_MDD=subject_labels[:,0][np.isin(subject_labels[:,1], 1)].copy()
    ix_train_HC=subject_labels[:,0][np.isin(subject_labels[:,1], 0)].copy()

    jsLog.w("Before deleting test subject")
    jsLog.w(ix_train_MDD)
    jsLog.w(ix_train_HC)

    ix_train_MDD=np.delete(ix_train_MDD, np.where(ix_train_MDD == ix_test))
    ix_train_HC=np.delete(ix_train_HC, np.where(ix_train_HC == ix_test))

    jsLog.w("After deleting test subject")
    jsLog.w(ix_train_MDD)
    jsLog.w(ix_train_HC)

    ### Second a one subject of each group is randomly selected
    ix_val=np.asarray([np.random.choice(ix_train_MDD), np.random.choice(ix_train_HC)], np.int64)

    ### Finally we delete this tho values from ix_train
    ix_train=np.delete(ix_train, np.isin(ix_train, ix_val))

    ### Mask for train
    mask_train=np.isin(sample_labels[:,0], ix_train)

    x_train=model_data[mask_train]
    y_train=sample_labels[mask_train,1]

    ### Mask for val
    mask_val=np.isin(sample_labels[:,0], ix_val)

    x_val=model_data[mask_val]
    y_val=sample_labels[mask_val,1]

   
    jsLog.w(f"TRAIN:\t T[{type(ix_train)}] [{ix_train.shape}]\n {ix_train}")
    jsLog.w(f"VAL:  \t T[{type(ix_val)}][{ix_val.shape}]\n {ix_val}")
    jsLog.w(f"TEST: \t T[{type(ix_test)}][{ix_test}]\n {ix_test}")

    jsLog.w(f"LEN SAMPLE_LABELS [{len(sample_labels)}] - LEN SUM [{len(x_train)+len(x_val)+len(x_test)}][{len(y_train)+len(y_val)+len(y_test)}]")

    # plot_mask(mask_test, mask_train, mask_val)

    jsLog.w("Statistics Before Norm:")
    
    jsLog.w(f"ALL DATA: [{model_data.max()}] - [{model_data.min()}] - [{model_data.mean()}] - [{model_data.std()}]")
    jsLog.w(f"TRAIN: [{x_train.max()}] - [{x_train.min()}] - [{x_train.mean()}] - [{x_train.std()}]")
    jsLog.w(f"VAL: [{x_val.max()}] - [{x_val.min()}] - [{x_val.mean()}] - [{x_val.std()}]")
    jsLog.w(f"TEST: [{x_test.max()}] - [{x_test.min()}] - [{x_test.mean()}] - [{x_test.std()}]")

    ## Normalization
    if normalization==0: # min_max normalization using only train data
        min_x = x_train.min()
        max_x = x_train.max()

        x_train=(x_train-min_x)/(max_x-min_x)
        x_val=(x_val-min_x)/(max_x-min_x)
        x_test=(x_test-min_x)/(max_x-min_x)

    elif normalization==1: # z-score normalization
        mean_x = x_train.mean()
        std_x = x_train.std()

        x_train=(x_train-mean_x)/std_x
        x_val=(x_val-mean_x)/std_x
        x_test=(x_test-mean_x)/std_x

    
    jsLog.w("Statistics After Norm:")
    
    jsLog.w(f"ALL DATA: [{len(sample_labels)}] - [{model_data.max()}] - [{model_data.min()}] - [{model_data.mean()}] - [{model_data.std()}]")
    jsLog.w(f"TRAIN: [{len(x_train)}][{len(y_train)}] - [{x_train.max()}] - [{x_train.min()}] - [{x_train.mean()}] - [{x_train.std()}]")
    jsLog.w(f"VAL: [{len(x_val)}][{len(y_val)}] - [{x_val.max()}] - [{x_val.min()}] - [{x_val.mean()}] - [{x_val.std()}]")
    jsLog.w(f"TEST: [{len(x_test)}][{len(y_test)}] - [{x_test.max()}] - [{x_test.min()}] - [{x_test.mean()}] - [{x_test.std()}]")

    jsLog.w(f"LEN SAMPLE_LABELS [{len(sample_labels)}] - LEN SUM [{len(x_train)+len(x_val)+len(x_test)}][{len(y_train)+len(y_val)+len(y_test)}]")

    return x_train, y_train, x_val, y_val, x_test, y_test

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        prog = 'Modma',
        description = 'Modma',
        epilog = 'Modma')

    parser.add_argument('-i', '--id')
    parser.add_argument('-m', '--model')
    args = parser.parse_args()
                        
    id_configuration=int(args.id)
    model_name=args.model

    jsLog=JSLog(f"{model_name}_ID_{id_configuration}")

    

    

    #p = CsvModelParams("MultiScaleCNN", id_configuration)
    p = CsvModelParams(model_name, id_configuration)

    model_data, sample_labels, subject_labels = load_data(jsLog, preprocessing_type=p.parameters["preprocessing_type"])

    print(f"SHAPES: [{model_data.shape}][{len(sample_labels)}]")

    sample_labels_st=sample_labels[:,[0,3]].astype(np.int64)

    print(f"Type SAMPLE_LABELS: {type(sample_labels_st[0,0])}")
    print(f"Type SUBJECT_LABELS: {type(subject_labels[0,0])}")

    print(sample_labels_st)
    
    metrics=[
        tf.keras.metrics.BinaryAccuracy(name="binary_accuracy"),
        tf.keras.metrics.Precision(name="precision"),
        tf.keras.metrics.Recall(name="recall"),
        tf.keras.metrics.TruePositives(name="true_positives"),
        tf.keras.metrics.TrueNegatives(name="true_negatives"),
        tf.keras.metrics.FalsePositives(name="false_positives"),
        tf.keras.metrics.FalseNegatives(name="false_negatives")
    ]

    callbacks=None
    num_iter=0
    num_fold=0

    # subject_labels(ix_subject, id_type_subject)
    for subject_label in subject_labels:
        
        index_subject=subject_label[0]
        type_subject=subject_label[1]

        start_time = time.time()
        print(f"{type(index_subject)} {index_subject}")
        x_train, y_train, x_val, y_val, x_test, y_test = get_train_val_test(jsLog, model_data, sample_labels_st, subject_labels, index_subject, normalization=1)

        if model_name=="MultiScaleCNN_LSTM":
            jsModel=MultiScaleCNN_LSTM(p)
        elif model_name=="EEGNet":
            jsModel=EEGNet(p)
        else:
            jsModel=MultiScaleCNN(p)
        
        jsModel.compile(
            optimizer=tf.keras.optimizers.Adam(),
            loss=tf.keras.losses.BinaryCrossentropy(),
            metrics=metrics
        )

        

        start_fit = time.time()
        history = jsModel.fit(
            x_train, 
            y_train, 
            batch_size=int(p.parameters["batch_size"]), 
            epochs=int(p.parameters["num_epochs"]), 
            validation_data=(x_val, y_val),
            callbacks=callbacks,
            verbose=2
        )
        end_fit = time.time()

        jsModel.summary()

        results = jsModel.evaluate(x_test, y_test, batch_size=int(p.parameters["batch_size"]), verbose=2)

        jsLog.save_history(
            p.get_params_name(), 
            p.get_params_values(end_fit - start_fit,num_iter, num_fold), 
            history)

        jsLog.save_results(
            p.get_params_name(),
            p.get_params_values(end_fit - start_fit,num_iter, num_fold),
            results)
        
        predictions_subject = jsModel.predict(x_test, batch_size=int(p.parameters["batch_size"]))
        jsLog.save_results_by_subject_type(
            p.get_params_name(),
            p.get_params_values(0,num_iter, num_fold),
            predictions_subject,
            index_subject,
            type_subject
        )
        
        end_time = time.time()

        
    

   
 